var urlParams;
var connection;

(window.onpopstate = function () {
    var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query))
       urlParams[decode(match[1])] = decode(match[2]);
})();

$(function() {

	doLog = function(aTextToLog, type){
		var aLog = $('<div>',{'class':"log "+type,html:aTextToLog});
		aLog.hide().prependTo($log).show('fast').delay(3000).hide('fast', function() { 
			$(this).remove(); 
		});
	}
	var $main = $("#Changing");
	var $metaNav = $("#metaNav");
	var $log = $("#userLog");

	/****************************************************/
	/**** AJAX UTILITIES FOR REQUESTS ****/
	/****************************************************/
	String.prototype.decodeHTML = function() {
		return $("<div>", {html: "" + this}).text();
	};
	var loadPage_request = null;
	afterLoad = function(html, textStatus, XMLHttpRequest) {
		$('#loading-page').addClass('fade');
		var replacementSelector = loadPage_request.replacementSelector;
		var push_state = loadPage_request.push_state;
		loadPage_request = null;
		if (textStatus == "error") {
			doLog('Sorry! An error occured when loading page content', 'error');
		}
		if (connection && $('#chat[data-global]').length == 0) { connection.disconnect(); }
		new_html = $.parseHTML(html, null, true);
		if (replacementSelector) {
			new_content = $(new_html).find(replacementSelector);
			$(replacementSelector).replaceWith(new_content);
		} else {
			/* new stylesheets and/or scripts */
			var new_combo_map_script = false;
			$(new_html).each(function(idx, elem) {
				if (elem.nodeName !== 'LINK' || elem.attributes['rel'].value !== 'stylesheet') return;
				if (elem.attributes['href'].value.indexOf('?') > -1) return;
				if ($('head link[rel="stylesheet"][href="' + elem.attributes['href'].value + '"]').length == 0) {
					$(elem).appendTo($('head'));
				}
			});
			$(new_html).each(function(idx, elem) {
				if (elem.nodeName !== 'SCRIPT') return;
				if (! elem.attributes || ! elem.attributes.src || ! elem.attributes.src.value) return;
				if (elem.attributes['src'].value.indexOf('?') > -1) return;
				if ($('head script[src="' + elem.attributes['src'].value + '"]').length == 0) {
					if (elem.attributes['src'].value.indexOf('combo.map.js') > -1) {
						new_combo_map_script = true;
					}
					$(elem).appendTo($('head'));
				}
			});

			/* new content */
			new_content = $(new_html).find('#Changing>*');
			$main.hide().empty().append(new_content).show();

			/* calling onpopstate here is necessary to get urlParams to be
			 * updated */
			window.onpopstate();

			canonical_href_node = $.grep($(new_html), function(elem, idx) {
				return (elem.nodeName === "LINK" && elem.attributes['rel'].value == "canonical");
			})[0];
			if (canonical_href_node) {
				canonical_href = canonical_href_node.attributes['href'].value + document.location.hash;
				try {
					history.replaceState({}, '', canonical_href);
				} catch(ex) {};
			}

			new_menu = $($.parseHTML(html)).find('#metaNav>*');
			$metaNav.empty().append(new_menu);

			var newTitle = html?html.match(/<title>(.*?)<\/title>/):'';
			if(newTitle){document.title = newTitle[1].trim().decodeHTML();}

			/* update body class */
			var body_class_match = html ? html.match(/<body.*class="(.*?)"/) : '';
			if (body_class_match) {
				document.body.setAttribute('class', body_class_match[1]);
			} else {
				document.body.setAttribute('class', '');
			}

			if (push_state !== false) {
				if (document.location.hash && $(document.location.hash).length) {
					$.scrollTo(document.location.hash, 1000, {offset: 0});
				} else if ($('body[data-scrolltop]').length) {
						$.scrollTo('body', 1000, {offset: 0});
				} else {
					var offset = -$('#metaNav').height() - 2;
					$.scrollTo('#Changing',1000,{offset: offset});
				}
			}

			$(document).trigger('combo:render-maps');
		}
		init();
		$(document).trigger('panikweb:load-page');

		if (typeof (Piwik) == 'object') {
			piwikTracker = Piwik.getAsyncTracker();
			if (typeof (piwikTracker.trackPageView) == 'function') {
				piwikTracker.setDocumentTitle(document.title);
				piwikTracker.setCustomUrl(window.location.href);
				piwikTracker.trackPageView();
				$('.audio a').each(function() {
					piwikTracker.addListener(this);
				});
			}
		}

	};

	function afterLoadError(xhr, text, error) {
		afterLoad(xhr.responseText, 'error', xhr);
	};

	$(window).on("popstate", function(e) {
		loadPage(location.href, false);
	});

	loadPage = function(href, push_state, replacementSelector) {
		if (push_state !== false) {
			history.pushState({}, '', href);
		}
		if (loadPage_request !== null) {
			loadPage_request.abort();
		}
		$('#loading-page').remove();
		$('<div id="loading-page"></div>').prependTo($('#All'));
		loadPage_request = $.ajax({
			url: href,
			success: afterLoad,
			error: afterLoadError,
			dataType: 'html'});
		loadPage_request.push_state = push_state;
		loadPage_request.replacementSelector = replacementSelector;
	};
        $.fn.ajaxifyClick = function(params) {
		this.each(function() {
			if ($(this).parents('.leaflet-control').length) {
				// don't touch leaflet control links
				return;
			}
			var href = $(this).attr("href");
			if (!href || href.match('\.(ogg|mp3|pdf|odt|ods|doc|xls|docx|xlsx|rtf|zip|rss|atom)$')) {
				// don't touch empty <a> or links to downloadable files
				return;
			}
			if ($(this).attr('target')) {
				// don't touch links with an explicit target
				return;
			}
			$(this).unbind('click');
			$(this).bind('click',function(e){
				var href = $(this).attr("href");
				if (e.which == 2) {
					window.open(href, '_blank');
					return false;
				}
				/* this checks the link points to a local document, be
				 * it because it's just a path, or because the server
				 * part points to the same domain */
				if (!href) {
					doLog('No href attributes, unable to load content','error');
					return false;
				}else if (!$(this).attr('target') && (
						href.indexOf(document.domain) > -1 ||href.indexOf(':') === -1 || href.indexOf(':') > 5
					)) {
					$(this).addClass('loading');
					if ($(this).data('replace')) {
						loadPage(href, false, $(this).data('replace'));
					} else {
						loadPage(href);
					}
					return false;
				}else{
					$(this).attr('target','_blank');
					$(this).attr('rel', 'noopener');
					return true;
				}
			});
		});
	};
	/****************************************************/
	/**** AUDIOPLAYER ****/
	/****************************************************/

	var timer = null;
	var ticker_interval = null;
	const players = document.querySelectorAll('.radio-player, #Player');

	players.forEach(function(player) {
		const whatsonair = player.querySelector('.radio-player--whatsonair') || document.getElementById('WhatsOnAir');
		const live_stream_controller = player.querySelector('.radio-player--live-stream-controller') || document.getElementById('LiveStreamController');
		const live_stream_button = player.querySelector('.radio-player--play-stream-button') || document.getElementById('streamSymbol');
		const live_stream_audio = player.querySelector('.radio-player--audio-stream') || document.getElementById('LiveStream');

		$(whatsonair).on('load',function(){
			var WhatsOnAir = $(this);

			const onair_json_url = player.dataset.onairUrl || '/onair.json';
			const display_emission_subtitle = player.dataset.hasOwnProperty('display_emission_subtitle');
			const display_episode_subtitle = player.dataset.hasOwnProperty('display_episode_subtitle');

			$.getJSON(onair_json_url, function(onair) {
				var onairContainer = $('<span>');
				if (onair.data.emission) {
					var text = onair.data.emission.title;
					if (onair.data.emission.subtitle && display_emission_subtitle) {
						text = text + " - " + onair.data.emission.subtitle;
					}
					$('<a>', {class: "onair--emission-link",
						  href: onair.data.emission.url,
						  text: text}
					).appendTo(onairContainer);
				}
				if (onair.data.episode) {
					var text = onair.data.episode.title;
					if (onair.data.episode.subtitle && display_episode_subtitle) {
						text = text + " - " + onair.data.episode.subtitle;
					}
					$('<span> - </span>').appendTo(onairContainer);
					$('<a>', {class: "onair--episode-link",
						  href: onair.data.episode.url,
						  text: text}
					).appendTo(onairContainer);
				}
				if (onair.data.nonstop) {
					if (onair.data.nonstop.url) {
						$('<a>', {class: "onair--nonstop-link",
							  href: onair.data.nonstop.url,
							  text: onair.data.nonstop.title}
						).appendTo(onairContainer);
					} else {
						$('<span>', {class: "onair--nonstop-span",
							     text: onair.data.nonstop.title}
						).appendTo(onairContainer);
					}
				}
				if (onair.data.track_title) {
					var $track_container = $('<span>', {class: "nonstop-track-metadata"});
					if (onair.data.nonstop && onair.data.nonstop.playlist_url) {
						$track_container = $('<a>', {class: "nonstop-track-metadata", href: onair.data.nonstop.playlist_url});
					}
					if (onair.data.track_artist) {
						$('<span>', {text: onair.data.track_artist, class: "nonstop-track-artist"}).appendTo($track_container);
						$('<span> - </span>').appendTo($track_container)
					}
					$('<span>', {text: onair.data.track_title,
						     class: "nonstop-track-title"}
					).appendTo($track_container);
					$('<span class="onair--nonstop-track-separator"> - </span>').appendTo(onairContainer);
					$track_container.appendTo(onairContainer);
				}
				if (onair.data.emission || onair.data.episode || onair.data.nonstop) {
					player.classList.remove('radio-player-nothing-playing')
					$('body').attr('data-nothing-playing', null)
					onairContainer.find('a').ajaxifyClick();
				} else {
					player.classList.add('radio-player-nothing-playing')
					onairContainer = $('<span class="nothing-playing"></span>');
					if (document.querySelectorAll('.nothing-playing').length == players.length) {
						$('body').attr('data-nothing-playing', 'true')
					} else {
						$('body').attr('data-nothing-playing', null)
					}
				}
				if (onair.data.emission && onair.data.emission.chat) {
					$('#CurrentlyChatting a').attr('href', onair.data.emission.chat);
					$('#CurrentlyChatting').show();
				} else {
					$('#CurrentlyChatting').hide();
				}
				var current_html = WhatsOnAir.find('> span').html();
				var new_html = onairContainer.html();
				if (new_html !== current_html) {
					WhatsOnAir.fadeOut();
					WhatsOnAir.empty().append(onairContainer);
					WhatsOnAir.fadeIn();
				}
				// update grid
				var cell_slug = (onair.data.emission && onair.data.emission.slug) || (
					onair.data.nonstop && onair.data.nonstop.slug);
				var $current_cell = $('.program li.current');
				if ($current_cell.data('program-slug') !== cell_slug &&
				    $current_cell.next().data('program-slug') === cell_slug) {
					$current_cell.removeClass('current').addClass('past');
					$current_cell.next().removeClass('future').addClass('current');
				}
			});
		});
		$(whatsonair).trigger('load');
		var refresh_onair_interval = 25000;
		setInterval(function() { $(whatsonair).trigger('load') }, refresh_onair_interval);

		$(live_stream_controller).on('click', function() {
			$(live_stream_button).trigger('click');
			return false;
		});
		$(live_stream_button).on('click keypress', function(e) {
			if (e.type === 'keypress' && !(e.key === ' ' || e.key === 'Enter'))
				return;
			e.preventDefault();
			var stream = live_stream_audio;
			if (stream.paused == false){
				stream.pause();
			}else{
				if (typeof (_paq) == 'object') {
					_paq.push(['trackEvent', 'Audio', 'Play Stream']);
				}
				stream.play();
			}
			return false;
		});
		$(live_stream_audio).on('play',function(){
			$('audio').each(function(idx, elem){
				if (elem != live_stream_audio) elem.pause();
			});
			$(live_stream_button).removeClass('icon-volume-up').addClass('icon-stop');
			if (typeof window.mixcloud_widgets !== 'undefined') {
			  window.mixcloud_widgets.forEach(function(value) { value.pause() });
			}
			if (typeof window.soundcloud_widgets !== 'undefined') {
			  window.souncloud_widgets.forEach(function(value) { value.pause() });
			}
		}).on('pause',function(){
			$(this).attr('src', '');  // invalid URI will stop stream for real
			$(this).attr('src', null);  // remove invalid attribute (will get back to <source> elements
			$(live_stream_button).addClass('icon-volume-up').removeClass('icon-stop');
		});
	})
	if($('#player-container').offset() && $('#mainHeader > div').length) {
		var topPosition = 0;
		topPosition = $('#mainHeader > div').offset().top + $('#mainHeader > div').height();
		$(window).bind('scroll load',function (event) {
			//$('#player-container').removeClass('fixed');
			var y = $(this).scrollTop() + 60;
			if (topPosition!== 0 && y >= topPosition) {
				$('#player-container').addClass('fixed').removeClass('normal');
			} else {
				$('#player-container').removeClass('fixed').addClass('normal');
			}
		});
	}

	if (typeof $().sortable !== 'undefined') {
	var $localList = $('#localList').playlist({
		controlContainer: $('<div>',{'class':"playListControls"}).sortable(),
		playlistContainer: $('<ol>',{id:"myPlaylist",'class':"custom"}).sortable(),
		onLoad:function(self){
			$('#toggleList').on('click',function(){ 
				self.playlistContainer.toggleClass('deploy');
			});
			$('#emptyList').on('click',function(){ 
				self._reset();
			});

			if(self.isActive){
				self.playlistContainer.scrollTo(self.isActive, 800 );
				self.isActive.find('audio').attr('preload',"preload")
			}
		},
		onPlay:function(self){
			$('#LiveStream')[0].pause();
			self.playlistContainer.scrollTo(self.isActive, 800 );
		},
		onAdd:function(self){
			//self.isLastAdd[0].scrollIntoView();
			self.isLastAdd.find('a').ajaxifyClick();
			self.playlistContainer.scrollTo(self.isLastAdd, 800).delay(1000).scrollTo(self.isActive, 800 ).clearQueue();

			if (typeof (_paq) == 'object') {
				_paq.push(['trackEvent', 'Audio', 'Add to playlist']);
			}
		},
		onUpdate:function(self){
			//doLog(JSON.stringify(self.playlist, null, '\t'));	
			if(self.playlist.length >= 1){
				self.element.show();
				$('#Player').addClass('withPlaylist').removeClass('withoutPlaylist');
			}else{
				self.element.hide();
				$('#Player').removeClass('withPlaylist').addClass('withoutPlaylist');
			}
		}
	});
	}

	init = function() {
		$("#All a, #All area").removeClass('loading');
		$("#All a, #All area").ajaxifyClick();
		$("#search-form").unbind('submit').on('submit', function(event) {
			event.preventDefault();
			$(this).addClass('loading');
			loadPage($(this).attr('action') + '?' + $(this).serialize());
		});
		// custom loading behaviour for week on homepage
		// (studioneau-only for now)
		$('#week .week-arrow a').unbind('click').on('click', function(event) {
			$.ajax({
				url: $(this).attr('href'),
				dataType: 'html',
				success: function(html, textStatus, jqXhr) {
					var $new_week = $(html);
					var $new_program_tabs = $new_week.find('.program.tabs');
					var $new_program_content = $new_week.find($new_program_tabs.attr('data-tab-about'));
					var $old_program_content = $($('.program.tabs').attr('data-tab-about'));
					$('.program.tabs').replaceWith($new_program_tabs);
					for (var i=0; i<$new_program_content.length; i++) {
						$old_program_content[i].replaceWith($new_program_content[i]);
					}
					init();
					$(document).trigger('panikweb:week-change');
				}
			});
			return false;
		});
		$(".tabs").each(function() {
			var self = $(this);
			var about = $($(this).attr("data-tab-about"));
			var current = $(this).find("[data-tab].active")[0];
			var dftShowSelector = current?".active":":first";
			var activeTab = $(this).find("[data-tab]"+dftShowSelector+"").addClass("active");
			$(this).find("[data-tab]").each(function() {
			    $(this).on('click load',function (e) {
				e.preventDefault();
				self.find(".active").removeClass("active");
				$(this).addClass("active");
				var new_active = $($(this).attr("data-tab"));
				if (! new_active.is(':visible')) {
				  about.find("[data-tabbed]").hide();
				  $($(this).attr("data-tab")).show();
				}
			    });
			});  
			activeTab.trigger('load');
		});
		// disable week tab buttons if there's nothing (but nonstop) on the day
		// (studioneau-only for now)
		$('.program.tabs [data-tab]').each(function(idx, elem) {
			if ($($(this).attr("data-tab")).find('a[href]').length == 0) {
				// nothing this day
				$(this).attr('disabled', 'disabled');
			}
		});
		$('[data-player-action]').on('click',function(){
			var audio = $('audio[data-sound-id=' + $(this).attr('data-sound-id') + ']');
			var sound_id = audio.data('sound-id');
			if($(this).attr('data-player-action') == "registerAudio"){
				$localList.playlist("registerAudio",audio);
			}else if($(this).attr('data-player-action') == "playAudio"){
				if ($(this).hasClass('icon-play-sign')) {
					$localList.playlist("registerAudio",audio);
					$localList.playlist("playSoundId", sound_id);
					if ($(this).parent().find('.icon-pause').length) {
						$(this).hide();
						$(this).parent().find('.icon-pause').show();
					}
				} else {
					$localList.playlist('pauseSounds');
				}
			}else if ($(this).attr('data-player-action') == "pauseSounds") {
				if ($(this).parent().find('.icon-play-sign').length) {
					$(this).hide();
					$(this).parent().find('.icon-play-sign').show();
				}
				$localList.playlist($(this).attr('data-player-action'));
			}else{
				$localList.playlist($(this).attr('data-player-action'));
			}
		});
		$('[data-player-control]').each(function(){
			var audio = $('audio[data-sound-id=' + $(this).attr('data-sound-id') + ']');
			$localList.playlist("bindControl",$(this).attr('data-player-control'),audio,$(this));
		});

		$('[data-highlight]').on('check',function(){
			$($(this).attr('data-about')).find($(this).attr('data-highlight')).addClass('highlighted').removeClass('normal');
		}).on('uncheck',function(){
			$($(this).attr('data-about')).find($(this).attr('data-highlight')).removeClass('highlighted').addClass('normal');
		}).on('click',function(){
			$(this).toggleClass('icon-check icon-check-empty');
			if($(this).hasClass('icon-check')){$(this).trigger('check');
			}else{	$(this).trigger('uncheck');}
		});
		$('[data-highlight].icon-check-empty').each(function(){
			$(this).trigger('uncheck');
		});
		$('[data-toggle]').on('check',function(){
			/* make sure all other unchecked items are hidden */
			$('[data-toggle].icon-check-empty').each(function() {
				$($(this).attr('data-about')).find($(this).attr('data-toggle')).hide();
			});
			$($(this).attr('data-about')).find($(this).attr('data-toggle')).show();
		}).on('uncheck',function(){
			$($(this).attr('data-about')).find($(this).attr('data-toggle')).hide();
			if ($('[data-toggle].icon-check').length == 0) {
				/* special case the situation where all toggles
				 * are unchecked, as we want that to mean
				 * "everything", not "nothing".
				 */
				$('[data-toggle].icon-check-empty').each(function() {
					$($(this).attr('data-about')).find($(this).attr('data-toggle')).show();
				});
			}
		}).on('click',function(){
			$(this).toggleClass('icon-check icon-check-empty');
			if($(this).hasClass('icon-check')){$(this).trigger('check');
			}else{	$(this).trigger('uncheck');}
		});
		$('[data-toggle].icon-check-empty').each(function(){
			$(this).trigger('uncheck');
		});

		initial_enabled_toggles = {};
		if (typeof(urlParams.q) == 'string') {
			urlParams.q.split('|').forEach(function(a) { initial_enabled_toggles[a] = 1; })
		}
		$('[data-toggle]').each(function() {
			if ($(this).data('toggle').substring(1) in initial_enabled_toggles) {
				$(this).trigger('click');
			}
		});

		$('[data-popup-href]').on('click', function() {
			$.ajax({
				url: $(this).data('popup-href'),
				success: function (html, textStatus, jqXhr) {
					$(html).appendTo($('body'));
				}
			});
			return false;
		});

		if ($('#search-form.big input#id_q').val() == '') {
			$('#search-form.big input#id_q').focus();
		}

		function select_roll_item(id) {
			$('#ticker li').attr('aria-hidden', 'true').css('opacity', 0);
			$('#ticker li a').attr('tabindex', '-1');
			$('#' + id).attr('aria-hidden', 'false').css('opacity', 1);
			$('#' + id + ' a').attr('tabindex', '0');
		}
		select_roll_item($('#ticker li:first').attr('id'));

		if (ticker_interval) clearInterval(ticker_interval);
		$('ticker a, #roller button').on('focus',function(e){
			clearInterval(ticker_interval);
			ticker_interval = null;
		});
		$('#roller button').on('click',function(e){
			clearInterval(ticker_interval);
			ticker_interval = null;
			e.preventDefault();
			select_roll_item($(this).attr('data-about'));
			$('#ticker li[aria-hidden=false] a').focus();
			return false;
		});
		ticker_interval = setInterval(function(){
			var next_item = $('#ticker li[aria-hidden=false]').next();
			if (! next_item.length) { next_item = $('#ticker li').first(); }
			select_roll_item($(next_item).attr('id'));
		}, 20000);

		function navsearch_click(event) {
			event.preventDefault();
			var query = $('#nav-search input').val();
			var form = $('#nav-search form');
			var href = '';
			if (query == '') {
				href = $(form).attr('action');
			} else {
				href = $(form).attr('action') + '?' + $(form).serialize();
			}
			if (event.which == 2) {
				window.open(href, '_blank');
			} else {
				$(this).addClass('loading');
				loadPage(href);
			}
			return false;
		}
		$('#nav-search a').unbind('click').on('click', navsearch_click);
		$('#nav-search form').unbind('submit').on('submit', navsearch_click);

		if ($('.bg-title').length) {
			var bg_title = $('<span id="bg-title" aria-hidden="true"></span>');
			bg_title.text($('.bg-title').text());
			$('#Changing').append(bg_title);
		}
		$('[data-toggle-img]').bind('click', function() {
			var src = $(this).data('toggle-img');
			$(this).data('toggle-img', $(this).attr('src'));
			$(this).attr('src', src);
			$(this).toggleClass('full-width');
		});

		$('.main .episode-detail audio, div.soundcell audio').each(function(index, audio) {
			var audio_src = $(audio).find('source')[0];
			var sound_id = $(audio).data('sound-id');
			var $waveform = $(audio).next();
			var waveform_url = audio_src.src.replace('.ogg', '.waveform.json').replace('.mp3', '.waveform.json');
			$.getJSON(waveform_url, function(data) {
				$waveform.empty();
				$waveform.append('<i class="duration">' + $waveform.data('duration-string') + '</i>');
				$.each(data, function(k, val) {
					var val = val * 0.5;
					$waveform.append('<span data-tick-index="' + k + '" style="height: ' + val + 'px;"></span>');
				});
				$waveform.show();
				$waveform.find('span').on('click', function() {
					/* if there's been something loaded */
					var matching_audio = $('audio[data-sound-id=' + sound_id + ']');
					if (matching_audio.length == 0) return;
					matching_audio = matching_audio[0];
					if (matching_audio.paused || matching_audio.ended) {
						$(this).parents('.sound').find('.icon-play-sign').click();
						return;
					}
					/* try to set time */
					var total_duration = parseFloat($waveform.data('duration'));
					var nb_ticks = $(this).parent().find('span').length;
					var tick_index = $(this).data('tick-index');
					matching_audio.currentTime = total_duration * tick_index / nb_ticks;
				});
			});
		});

		$('#nav-language a').click(function() {
			document.cookie = 'panikweb_language=' + $(this).data('lang') + '; path=/';
			window.location = window.location;
			return false;
		});

		if ($('.sound + .content .text	').length) {
			var text_content = $('.sound + .content .text')[0];
			text_content.innerHTML = text_content.innerHTML.replace(
				/[0-9][0-9]+:[0-9][0-9]/g,
				function(x) { return '<span class="timestamp">' + x + "</span>"; });
			$(text_content).find('span.timestamp').on('click', function() {
				var $waveform = $('div.waveform').first();
				var sound_id = $waveform.prev().data('sound-id');
				var total_duration = parseFloat($waveform.data('duration'));
				var nb_ticks = $waveform.find('span').length;
				var timestamp = $(this).text().split(':');
				var timestamp_position = timestamp[0] * 60 + timestamp[1] * 1;
				var tick_idx = parseInt(nb_ticks * timestamp_position / total_duration);
				// play, then set rough position
				$('.episode.detail .sound .icon-play-sign').first().trigger('click');
				var matching_audio = $('audio[data-sound-id=' + sound_id + ']');
				matching_audio[0].currentTime = timestamp_position;
			});
		}

		if (document.cookie.indexOf('panikdb=on') != -1) {
			panikdb_path = null;
			if (window.location.pathname.indexOf('/emissions/') == 0) {
				panikdb_path = window.location.pathname;
			} else if (window.location.pathname.indexOf('/news/') == 0) {
				panikdb_path = '/emissions' + window.location.pathname;
			}
			if (panikdb_path) {
				$('<a id="panikdb" href="http://panikdb.radiopanik.org' + panikdb_path + '">Voir dans PanikDB</a>').appendTo($main);
			}
		}

		$('.gallery').each(function() {
		  var $gallery = $(this);
		  $gallery.find('span.image').on('click', function() {
		    if ($(this).find('img').hasClass('portrait')) {
			$(this).parents('.gallerycell').addClass('portrait');
		    } else {
			$(this).parents('.gallerycell').removeClass('portrait');
		    }
		    $gallery.find('div.first img').attr('src', $(this).data('image-large'));
		    $gallery.find('div.first span.gallery-legend').text($(this).find('img').attr('title') || '');
		    $gallery.find('div.first').show('fade');
		    $gallery.find('button').show();
		    return false;
		  });
		  $gallery.find('div.first').on('click', function() {
			  $(this).parents('.gallery').find('button').hide();
			  $(this).toggle('fade');
			  return false;
		  });
		});

		document.querySelectorAll('details[id]').forEach(
			el => {
				const key = 'nav-folded-id-' + el.id
				if (localStorage[key] !== undefined) el.open = (localStorage[key] === "true")
				el.addEventListener('toggle', (e) => localStorage[key] = el.open)
			}
		)

                /* CHAT */
                if ($('#chat').length && ! $('#player-container.on-chat-page').length) {
                    $('#player-container').addClass('on-chat-page');
                    var $msg = $('input#msg');
                    var $send = $('button#send');
                    var chat_roster = Object();

                    function enable_moderation() {
                      $('#chat').addClass('moderation');
                      $('#chat').on('click', 'span.from', function() {
                        var name = $(this).text();
                        if (confirm('Kick ' + name + ' ?')) {
                          var muc = $('div#chat').data('chatroom');
                          connection.muc.kick(muc + '@conf.panik', name,
                                          'no reason',
                                          function(iq) {
                                          },
                                          function(iq) {
                                            doLog('error kicking', 'error');
                                          }
                          );
                        }
                      });
                    }

                    $('.nick input').on('keydown', function(ev) {
                        if (ev.keyCode == 13) {
                            $('.nick button').trigger('click');
                            return false;
                        }
                        return true;
                    });

                    $('.nick button').on('click', function() {
                      window.localStorage['pa-nick'] = $('.nick input').val();
                      var nick = window.localStorage['pa-nick'];
                      $('.commands .prompt').text(nick);

                      connection = new Strophe.Connection("/http-bind");
                      connection.connect('im.panik', null, function(status, error) {
                        if (status == Strophe.Status.CONNECTING) {
                            $('.nick').show();
                            $('.commands').hide();
                            //console.log('Strophe is connecting.');
                        } else if (status == Strophe.Status.CONNFAIL) {
                            $('.nick').show();
                            $('.commands').hide();
                            //console.log('Strophe failed to connect.');
                        } else if (status == Strophe.Status.DISCONNECTING) {
                            $('.nick').show();
                            $('.commands').hide();
                            //console.log('Strophe is disconnecting.');
                        } else if (status == Strophe.Status.DISCONNECTED) {
                            $('.nick').show();
                            $('.commands').hide();
                            //console.log('Strophe is disconnected.');
                        } else if (status == Strophe.Status.CONNECTED) {
                            //console.log('Strophe is connected');
                            $('.nick').hide();
                            $('.commands').show();
                            var jid = nick;
                            var muc = $('div#chat').data('chatroom');
                            connection.muc.join(muc + '@conf.panik', jid,
                                    function(msg) {
                                        if (! msg.textContent) return true;
                                        var from = msg.attributes.from.value.replace(/.*\//, '');
                                        var klass = '';
                                        if (from == jid) {
                                            klass = 'msg-out';
                                        } else {
                                            klass = 'msg-in';
                                        }
                                        var new_msg = $('<div class="msg new ' + klass + '"><span class="from">' + from + '</span> <span class="content">' + msg.textContent + '</span></div>').prependTo($('#chat'));
                                        new_msg[0].offsetHeight; /* trigger reflow */
                                        new_msg.removeClass('new');
                                        $('div#chat div:nth-child(20)').remove()
                                        return true;
                                    },
                                    function(pres) {
                                            var nick = $('.nick input').val()
                                            var muc = $('div#chat').data('chatroom');
                                            if (pres.getElementsByTagName('status').length &&
                                                pres.getElementsByTagName('status')[0].attributes &&
                                                pres.getElementsByTagName('status')[0].attributes.code &&
                                                pres.getElementsByTagName('status')[0].attributes.code.value == '307') {
                                              /* kicked */
                                              try {
                                                var kicked = pres.getElementsByTagName('item')[0].attributes.nick.value;
                                              } catch (error) {
                                                var kicked = pres.attributes.from.value.split('/')[1];
                                              }
                                              if (kicked == nick) {
                                                connection.disconnect();
                                                $('div.nick').css('visibility', 'hidden');
                                                var new_msg = $('<div class="msg info new"><span class="content">You have been kicked out.</span></div>').prependTo($('#chat'));
                                              } else {
                                                var new_msg = $('<div class="msg info new"><span class="content">' + kicked + ' a été mis dehors.</span></div>').prependTo($('#chat'));
                                              }
                                              new_msg[0].offsetHeight; /* trigger reflow */
                                              new_msg.removeClass('new');
                                            }
                                            if (pres.getElementsByTagName('conflict').length == 1) {
                                              $('.nick input').val(nick + '_');
                                              connection.disconnect();
                                              if (nic.indexOf('__') == -1) {
                                                // auto reconnect unless nick
                                                // has already been altered
                                                // after conflicts.
                                                $('.nick button').trigger('click');
                                              }
                                            }
                                            return true;
                                    },
                                    function(roster) {
                                            if (chat_roster[nick] == true) {
                                                for (contact in roster) {
                                                        if (chat_roster[contact] !== true) {
                                                                var new_msg = $('<div class="msg info new joined"><span class="content">' + contact + ' est dans la place.</span></div>').prependTo($('#chat'));
                                                                new_msg[0].offsetHeight; /* trigger reflow */
                                                                new_msg.removeClass('new');
                                                        }
                                                }
                                            }
                                            chat_roster = Object();
                                            for (contact in roster) {
                                                chat_roster[contact] = true;
                                            }
                                            return true;
                                    }
                                    );
                            }
                         });

                    });

                    function send() {
                        var text = $msg.val();
                        if (! text) {
                            $msg.focus();
                            return true;
                        }
                        var muc = $('div#chat').data('chatroom');
                        if (text.startsWith('/auth')) {
                            $msg.val('');
                            /* ideally it would trigger some code on the server
                             * to check the password and elevate priviledges in
                             * prosody. It doesn't, this is security theater.
                             */
                            $.getJSON('/media/chat.json', function(chat_info) {
                              if (text.split(' ')[1] == chat_info.secret) {
                                enable_moderation();
                              }
                            });
                            return true;
                        }
                        connection.muc.message(muc + '@conf.panik', null, text);
                        $msg.val('');
                        return true;
                    }
                    $send.click(send);
                    $msg.keydown(function(ev) {
                        if (ev.keyCode == 13) {
                            send();
                            return false;
                        }
                        return true;
                    });

                    if (window.localStorage['pa-nick'] !== undefined) {
                      $('.nick input').val(window.localStorage['pa-nick']);
                      if ($('#chat[data-global]').length == 0) {
                        $('.nick button').click();
                      }
                    }

                    $(window).on('beforeunload', function() {
                        if (connection && $('#chat[data-global]').length == 0) { connection.disconnect(); }
                    });

                } else if ($('#chat').length == 0) {
                    $('#player-container').removeClass('on-chat-page');
                }
	}
	window.panikweb_init_page = init;
	init();

	if (! document.createElement('audio').canPlayType('audio/ogg') &&
		document.createElement('audio').canPlayType('audio/mpeg') ) {
		$('#ogg-m3u').hide();
		$('#mp3-m3u').show();
	}

	if (typeof Konami !== 'undefined') {
	var konami = new Konami('/party');
	}

	$(document).on('panik:play', function(ev, data) {
		var $page_audio_controls = $('.main').find('div.audio[data-sound-id="' + data.sound_id + '"]');
		$page_audio_controls.find('.icon-play-sign').removeClass('icon-play-sign').addClass('icon-pause');
	});

	$(document).on('panik:pause', function(ev, data) {
		var $page_audio_controls = $('.main').find('div.audio[data-sound-id="' + data.sound_id + '"]');
		$page_audio_controls.find('.icon-pause').removeClass('icon-pause').addClass('icon-play-sign');
	});


	$(document).on('panik:timeupdate', function(ev, data) {
		var $page_audio_controls = $('.main').find('div.audio[data-sound-id="' + data.sound_id + '"]');
		$page_audio_controls.find('.icon-play-sign').removeClass('icon-play-sign').addClass('icon-pause');
		$waveform = $('.main div.waveform[data-sound-id="' + data.sound_id + '"]');
		var elems = $waveform.find('span');
		var total_elems = elems.length;
		var done = total_elems * data.position;
		$waveform.find('span').each(function(k, elem) {
		  if (k < done) {
			$(elem).addClass('done').removeClass('current');
		  } else {
			$(elem).removeClass('done');
		  }
		});
		$waveform.find('span.done:last').addClass('current');
	});

	$('.gallery-previous').on('click', function() {
	  var e = $.Event('keydown');
	  e.which = 37;
	  $('body').trigger(e);
	  return false;
	});

	$('.gallery-next').on('click', function() {
	  var e = $.Event('keydown');
	  e.which = 39;
	  $('body').trigger(e);
	  return false;
	});

	$('.gallery-close').on('click', function() {
	  var e = $.Event('keydown');
	  e.which = 27;
	  $('body').trigger(e);
	  return false;
	});


	$("body").keydown(function(e) {
	  var $visible_element = $('div.first:visible img');
	  if ($visible_element.length == 0) {
	    return true;
	  }
	  if ($visible_element.length > 1) {
	    /* remove all but last */
	    $visible_element.parent().find('img:not(:last)').remove();
	  }
	  var $visible_element = $('div.first:visible img');
	  var img_url = $visible_element.attr('src');
	  var all_img = $('div.gallery span[data-image-large] img');
	  var active_img = $('div.gallery span[data-image-large="' + img_url + '"] img');
	  var idx = all_img.index(active_img);
	  if (e.which == 37) { // left
	    idx--;
	    if (idx == -1) {
	      idx = all_img.length-1;
	    }
	  } else if (e.which == 39) { // right
	    idx++;
	    if (idx == all_img.length) {
	      idx = 0;
	    }
	  } else if (e.which == 27) { // escape
	    $visible_element.parents('.first').toggle('fade');
	    $visible_element.parents('.gallery').find('button').hide();
	    return true;
	  } else {
	    return true;
	  }
	  /* create a new <img> with the new image but opacity 0, then display
	   * it using a css transition */
	  if (e.which == 37) { $visible_element.css('transform-origin', 'bottom right'); }
	  if (e.which == 39) { $visible_element.css('transform-origin', 'bottom left'); }
	  var new_img = $visible_element.clone().appendTo($visible_element.parent());
	  $visible_element.hide();
	  $(new_img).css('opacity', 0).attr('src', $(all_img[idx]).parent().data('image-large'));
	  $(new_img).css('transform', 'scale(0, 1)');
	  $(new_img)[0].offsetHeight; /* trigger reflow */
	  $(new_img).css('opacity', 1).css('transform', 'scale(1)');
	  $(new_img).parents('.gallery').find('span.gallery-legend').text($(all_img[idx]).attr('title') || '');
	  if ($(all_img[idx]).hasClass('portrait')) {
	    if (! $(new_img).parents('.gallerycell').hasClass('portrait')) {
	      $visible_element.parent().find('img:not(:last)').remove();
	      $(new_img).parents('.gallerycell').addClass('portrait');
	    }
	  } else {
	    if ($(new_img).parents('.gallerycell').hasClass('portrait')) {
	      $visible_element.parent().find('img:not(:last)').remove();
	      $(new_img).parents('.gallerycell').removeClass('portrait');
	    }
	  }
	  return false;
	});
});
