import haystack.backends.solr_backend
from django import forms
from django.conf import settings
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.list import ListView
from emissions.models import Emission, Episode, Format, NewsCategory, NewsItem, SoundFile
from haystack.backends import EmptyResults
from haystack.constants import DJANGO_CT, DJANGO_ID, ID
from haystack.query import RelatedSearchQuerySet, SearchQuerySet
from haystack.utils import get_identifier
from pysolr import SolrError
from taggit.models import Tag


class MoreLikeThisSearchQuerySet(SearchQuerySet):
    def more_like_this(self, model_instance, **kwargs):
        clone = self._clone()
        clone.query.more_like_this(model_instance, **kwargs)
        return clone


class CustomSolrSearchQuery(haystack.backends.solr_backend.SolrSearchQuery):
    def more_like_this(self, model_instance, **kwargs):
        self._more_like_this = True
        self._mlt_instance = model_instance

    def run_mlt(self, **kwargs):
        """Builds and executes the query. Returns a list of search results."""
        if self._more_like_this is False or self._mlt_instance is None:
            raise MoreLikeThisError("No instance was provided to determine 'More Like This' results.")

        additional_query_string = self.build_query()
        search_kwargs = {
            'start_offset': self.start_offset,
            'result_class': self.result_class,
            'models': self.models,
            'mlt.fl': 'tags',
        }

        if self.end_offset is not None:
            search_kwargs['end_offset'] = self.end_offset - self.start_offset

        results = self.backend.more_like_this(self._mlt_instance, additional_query_string, **search_kwargs)
        self._results = results.get('results', [])
        self._hit_count = results.get('hits', 0)


class CustomSolrSearchBackend(haystack.backends.solr_backend.SolrSearchBackend):
    def more_like_this(
        self,
        model_instance,
        additional_query_string=None,
        start_offset=0,
        end_offset=None,
        models=None,
        limit_to_registered_models=None,
        result_class=None,
        **kwargs,
    ):
        from haystack import connections

        # Deferred models will have a different class ("RealClass_Deferred_fieldname")
        # which won't be in our registry:
        model_klass = model_instance._meta.concrete_model

        index = connections[self.connection_alias].get_unified_index().get_index(model_klass)
        field_name = index.get_content_field()
        params = {
            'fl': '*,score',
            'mlt.fl': 'text,title',
            'mlt.qf': 'text^0.3 tags^2.0 title^1.0',
        }

        if start_offset is not None:
            params['start'] = start_offset

        if end_offset is not None:
            params['rows'] = end_offset

        narrow_queries = set()

        if limit_to_registered_models is None:
            limit_to_registered_models = getattr(settings, 'HAYSTACK_LIMIT_TO_REGISTERED_MODELS', True)

        if models and len(models):
            model_choices = sorted(
                '%s.%s' % (model._meta.app_label, model._meta.model_name) for model in models
            )
        elif limit_to_registered_models:
            # Using narrow queries, limit the results to only models handled
            # with the current routers.
            model_choices = self.build_models_list()
        else:
            model_choices = []

        if len(model_choices) > 0:
            if narrow_queries is None:
                narrow_queries = set()

            narrow_queries.add('%s:(%s)' % (DJANGO_CT, ' OR '.join(model_choices)))

        if additional_query_string:
            narrow_queries.add(additional_query_string)

        if narrow_queries:
            params['fq'] = list(narrow_queries)

        query = "%s:%s" % (ID, get_identifier(model_instance))

        try:
            raw_results = self.conn.more_like_this(query, field_name, **params)
        except (OSError, SolrError) as e:
            if not self.silently_fail:
                raise

            self.log.error("Failed to fetch More Like This from Solr for document '%s': %s", query, e)
            raw_results = EmptyResults()

        return self._process_results(raw_results, result_class=result_class)


haystack.backends.solr_backend.SolrEngine.query = CustomSolrSearchQuery
haystack.backends.solr_backend.SolrEngine.backend = CustomSolrSearchBackend

import haystack.views
from haystack.forms import FacetedSearchForm, SearchForm
from haystack.views import FacetedSearchView, search_view_factory


class GlobalSearchForm(FacetedSearchForm):
    def no_query_found(self):
        sqs = super().no_query_found()
        if self.selected_facets:
            sqs = self.searchqueryset.all()
            for facet in self.selected_facets:
                if ":" not in facet:
                    continue
                field, value = facet.split(":", 1)
                if value:
                    sqs = sqs.narrow('%s:"%s"' % (field, sqs.query.clean(value)))
        return sqs


class SearchView(FacetedSearchView):
    def extra_context(self):
        context = super().extra_context()
        if self.request.GET.getlist('selected_facets'):
            context['facets_qs'] = '&selected_facets=' + '&'.join(self.request.GET.getlist('selected_facets'))
        context['selected_categories'] = [
            x.split(':', 1)[1]
            for x in self.request.GET.getlist('selected_facets')
            if x.startswith('categories_exact') and ':' in x
        ]
        context['selected_tags'] = [
            x.split(':', 1)[1]
            for x in self.request.GET.getlist('selected_facets')
            if x.startswith('tags_exact') and ':' in x
        ]
        if 'categories' in context['facets'].get('fields', []):
            context['facets']['fields']['categories'] = [
                x for x in context['facets']['fields']['categories'] if x[1] > 0
            ]
            context['facets']['fields']['categories'].sort()
        if 'tags' in context['facets'].get('fields', []):
            context['facets']['fields']['tags'] = [x for x in context['facets']['fields']['tags'] if x[1] > 0]
        return context


sqs = SearchQuerySet().models(Emission, Episode, NewsItem).facet('categories').facet('tags')

view = search_view_factory(SearchView, form_class=GlobalSearchForm, searchqueryset=sqs)


class ListenArchivesForm(FacetedSearchForm):
    q = forms.CharField(required=False, label='')

    def no_query_found(self):
        return self.searchqueryset.all()

    def search(self):
        sqs = super().search()
        return sqs.load_all()


class ListenArchivesView(FacetedSearchView):
    template = 'listen/archives.html'

    def __init__(self):
        sqs = RelatedSearchQuerySet().models(SoundFile).facet('format').facet('tags').order_by('-date')
        super().__init__(searchqueryset=sqs, form_class=ListenArchivesForm, results_per_page=20)

    def extra_context(self):
        context = super().extra_context()
        if self.request.GET.getlist('selected_facets'):
            context['facets_qs'] = '&selected_facets=' + '&'.join(self.request.GET.getlist('selected_facets'))
        context['selected_format'] = [
            x.split(':', 1)[1]
            for x in self.request.GET.getlist('selected_facets')
            if x.startswith('format_exact')
        ]
        if 'format' in context['facets'].get('fields', []):
            context['facets']['fields']['format'].sort()
        return context


listenArchives = search_view_factory(ListenArchivesView)


class SoundFileSearchView(ListView):
    template_name = 'soundfiles/search.html'
    paginate_by = 20

    def get_queryset(self):
        qs = SoundFile.objects.published()
        if self.request.GET.get('format'):
            qs = qs.filter(format__slug=self.request.GET.get('format'))
        if self.request.GET.get('q'):
            vector = (
                SearchVector('title', config=settings.FTS_DICTIONARY_CONFIG, weight='A')
                + SearchVector('episode__title', config=settings.FTS_DICTIONARY_CONFIG, weight='A')
                + SearchVector('episode__text', config=settings.FTS_DICTIONARY_CONFIG, weight='B')
                + SearchVector('episode__emission__title', config=settings.FTS_DICTIONARY_CONFIG, weight='C')
            )
            query = SearchQuery(self.request.GET.get('q', ''), config=settings.FTS_DICTIONARY_CONFIG)
            qs = qs.annotate(rank=SearchRank(vector, query)).filter(rank__gte=0.1).order_by('-rank')
        else:
            qs = qs.distinct().order_by('-episode__first_diffusion_date')
        qs = qs.select_related()
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['formats'] = Format.objects.filter(archived=False)
        context['selected_format'] = self.request.GET.get('format')
        context['query'] = self.request.GET.get('q')
        return context


soundfiles_search = SoundFileSearchView.as_view()


class NewsArchivesForm(FacetedSearchForm):
    q = forms.CharField(required=False, label='')

    def no_query_found(self):
        return self.searchqueryset.all()

    def search(self):
        sqs = super().search()
        return sqs.load_all()


class NewsArchivesView(FacetedSearchView):
    template = 'news/archives.html'

    def __init__(self):
        sqs = (
            RelatedSearchQuerySet().models(NewsItem).facet('news_categories').facet('tags').order_by('-date')
        )
        super().__init__(searchqueryset=sqs, form_class=NewsArchivesForm, results_per_page=20)

    def extra_context(self):
        context = super().extra_context()
        if self.request.GET.getlist('selected_facets'):
            context['facets_qs'] = '&selected_facets=' + '&'.join(self.request.GET.getlist('selected_facets'))
        context['selected_news_categories'] = [
            x.split(':', 1)[1]
            for x in self.request.GET.getlist('selected_facets')
            if x.startswith('news_categories_exact')
        ]
        context['selected_tags'] = [
            x.split(':', 1)[1]
            for x in self.request.GET.getlist('selected_facets')
            if x.startswith('tags_exact')
        ]
        if 'news_categories' in context['facets'].get('fields', []):
            context['facets']['fields']['news_categories'].sort(key=lambda x: slugify(x))
        if 'tags' in context['facets'].get('fields', []):
            context['facets']['fields']['tags'] = [x for x in context['facets']['fields']['tags'] if x[1] > 0]
        return context


newsArchives = search_view_factory(NewsArchivesView)


class GlobalSearchView(ListView):
    template_name = 'search/global_search.html'
    paginate_by = 20

    def get_queryset(self):
        qs = Episode.objects.all()
        if self.selected_tag:
            qs = qs.filter(tags__in=[self.selected_tag.id])
        if self.request.GET.get('q'):
            vector = (
                SearchVector('title', config=settings.FTS_DICTIONARY_CONFIG, weight='A')
                + SearchVector('text', config=settings.FTS_DICTIONARY_CONFIG, weight='B')
                + SearchVector('emission__title', config=settings.FTS_DICTIONARY_CONFIG, weight='C')
            )
            query = SearchQuery(self.request.GET.get('q', ''), config=settings.FTS_DICTIONARY_CONFIG)
            qs = qs.annotate(rank=SearchRank(vector, query)).filter(rank__gte=0.1).order_by('-rank')
        qs = qs.select_related()
        if not self.request.GET.get('q'):
            qs = qs.order_by('-first_diffusion_date')
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['query'] = self.request.GET.get('q')
        context['selected_tag'] = self.selected_tag
        return context

    def get(self, request, *args, **kwargs):
        selected_facet = self.request.GET.get('selected_facets') or ''
        self.selected_tag = None
        if selected_facet.startswith('tags_exact:'):
            self.selected_tag = Tag.objects.filter(slug=selected_facet.split(':', 1)[1]).first()
        return super().get(request, *args, **kwargs)


global_search = GlobalSearchView.as_view()


class NewsItemSearchView(ListView):
    template_name = 'news/search.html'
    paginate_by = 20

    def get_queryset(self):
        qs = NewsItem.objects.all()
        if self.request.GET.get('category'):
            qs = qs.filter(category__slug=self.request.GET.get('category'))
        if self.request.GET.get('q'):
            vector = (
                SearchVector('title', config=settings.FTS_DICTIONARY_CONFIG, weight='A')
                + SearchVector('text', config=settings.FTS_DICTIONARY_CONFIG, weight='B')
                + SearchVector('emission__title', config=settings.FTS_DICTIONARY_CONFIG, weight='C')
            )
            query = SearchQuery(self.request.GET.get('q', ''), config=settings.FTS_DICTIONARY_CONFIG)
            qs = qs.annotate(rank=SearchRank(vector, query)).filter(rank__gte=0.1).order_by('-rank')
        qs = qs.select_related()
        qs = qs.order_by('-date')
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = NewsCategory.objects.filter(archived=False)
        context['selected_category'] = self.request.GET.get('category')
        context['query'] = self.request.GET.get('q')
        return context


newsitems_search = NewsItemSearchView.as_view()
