from django.db.models import Count
from django.http import JsonResponse

from .models import PodcastLogLine


def downloads_json(request, *args, **kwargs):
    qs = PodcastLogLine.objects.filter(is_bot=False).values('soundfile').annotate(total=Count('*'))
    content = {x['soundfile']: x['total'] for x in qs}
    return JsonResponse(content)
