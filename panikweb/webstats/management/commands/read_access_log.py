import os
import re
from ipaddress import ip_address

import dateutil.parser
import requests
from django.conf import settings
from django.core.management.base import BaseCommand
from emissions.models import SoundFile

from panikweb.webstats.models import PodcastLogLine


class Sighting:
    ip = None
    path = None
    user_agent = None
    log_datetime = None
    size = 0
    stored = False

    def __init__(self, ip, path, user_agent, log_datetime, size):
        self.ip = ip
        self.path = path
        self.user_agent = user_agent
        self.log_datetime = log_datetime
        self.size = int(size)

    def seen_key(self):
        return (self.ip, self.path, self.user_agent)


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        verbose = bool(kwargs.get('verbosity') > 0)
        regex = re.compile(
            r'([(a-f\d\.\:)]+) - - \[(.*?)\] "GET /media/sounds/(.*?) HTTP/..." \d+ (\d+) "(.*?)" "(.*?)"'
        )
        seen = {}
        sizes_cache = {}
        with open(settings.ACCESS_LOG_FILENAME) as fd:
            for line in fd:
                match = regex.match(line)
                if not match:
                    continue
                ip, date, path, size, referrer, user_agent = match.groups()
                if not (path.endswith('.ogg') or path.endswith('.mp3')):
                    continue
                log_datetime = dateutil.parser.parse(date.replace(':', ' ', 1))
                sighting = Sighting(ip, path, user_agent, log_datetime, size)
                previous_sighting = seen.get(sighting.seen_key())
                if previous_sighting:
                    if previous_sighting.stored:
                        # skip sighting that has already been stored
                        continue
                    # most likely partial downloads, add to previous
                    previous_sighting.size += sighting.size
                    sighting = previous_sighting
                real_size = sizes_cache.get(path)
                if real_size == -1:
                    continue
                if not real_size:
                    try:
                        real_size = os.stat(os.path.join(settings.MEDIA_ROOT, 'sounds', path)).st_size
                    except OSError:
                        # maybe the file was offloaded, try to get size using a HEAD request
                        if settings.OFFSITE_MEDIA_SOUNDS and settings.OFFSITE_BASE_URL:
                            try:
                                real_size = int(
                                    requests.head(
                                        f'{settings.OFFSITE_BASE_URL}sounds/{path}', timeout=10
                                    ).headers['content-length']
                                )
                            except requests.RequestException:
                                pass
                    if not real_size:
                        sizes_cache[path] = -1
                        continue
                    sizes_cache[path] = real_size
                seen[sighting.seen_key()] = sighting
                if sighting.size < real_size / 5:
                    # don't record under downloaded size threshold
                    continue
                try:
                    soundfile = SoundFile.objects.get(id=path.split('_')[-3])
                except SoundFile.DoesNotExist:
                    continue

                obj, created = PodcastLogLine.objects.update_or_create(
                    timestamp=sighting.log_datetime,
                    ip=self.anonymise_ip(sighting.ip),
                    path=sighting.path,
                    soundfile=soundfile,
                    user_agent=sighting.user_agent,
                    defaults={
                        'referrer': referrer,
                    },
                )
                if obj.is_bot is None:
                    obj.check_bot()
                sighting.stored = True

    def anonymise_ip(self, ip):
        address = ip_address(ip)
        if address.version == 4:  # apply 255.255.255.0 mask
            address = ip_address(address.packed[:3] + b'\0')
        elif address.version == 6:  # apply ffff:ffff:ffff:ffff:0000:0000:0000:0000 mask
            address = ip_address(address.packed[:8] + b'\0' * 8)
        return str(address)
