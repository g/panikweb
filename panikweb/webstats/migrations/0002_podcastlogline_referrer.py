# Generated by Django 1.11.29 on 2020-12-13 16:59

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('webstats', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='podcastlogline',
            name='referrer',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
