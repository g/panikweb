# Generated by Django 1.11.29 on 2020-12-13 15:18

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('emissions', '0015_auto_20200404_1510'),
    ]

    operations = [
        migrations.CreateModel(
            name='PodcastLogLine',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('timestamp', models.DateTimeField()),
                ('ip', models.GenericIPAddressField()),
                ('path', models.CharField(max_length=255)),
                ('user_agent', models.CharField(max_length=255)),
                ('is_bot', models.NullBooleanField()),
                (
                    'soundfile',
                    models.ForeignKey(
                        null=True, on_delete=django.db.models.deletion.SET_NULL, to='emissions.SoundFile'
                    ),
                ),
            ],
        ),
    ]
