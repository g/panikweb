from django.db import models


class PodcastLogLine(models.Model):
    timestamp = models.DateTimeField()
    ip = models.GenericIPAddressField()
    path = models.CharField(max_length=1000)
    soundfile = models.ForeignKey('emissions.SoundFile', null=True, on_delete=models.SET_NULL)
    user_agent = models.CharField(max_length=1000)
    referrer = models.CharField(max_length=1000, null=True)
    is_bot = models.BooleanField(null=True)

    def check_bot(self, recheck=False):
        if self.is_bot is not None and recheck is False:
            return
        BOTS = [
            'AdsBot-Google',
            'AdsBot-Google-Mobile',
            'Applebot/',
            'archive.org_bot',
            'bingbot/',
            'Googlebot/',
            'Googlebot-Image/',
            'Googlebot-Video/',
            'INA dlweb',
            'MJ12bot/',
            'MTRobot/',
            'PetalBot',
            'SMTBot/',
            'special_archiver/',
            'bnf.fr_bot',
            'Orbbot',
        ]
        current_value = self.is_bot
        for bot in BOTS:
            if bot in self.user_agent:
                self.is_bot = True
                break
        else:
            if 'bot' in self.user_agent:
                self.is_bot = None
            else:
                self.is_bot = False
        if self.is_bot != current_value:
            self.save()
