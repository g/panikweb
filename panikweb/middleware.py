import re

from django.utils.deprecation import MiddlewareMixin


class StripPiwikCookieMiddleware(MiddlewareMixin):
    # idea from http://djangosnippets.org/snippets/1772/
    strip_re = re.compile(r'(_pk_[a-z0-9\.]+=.+?(?:; |$))')

    def process_request(self, request):
        try:
            cookie = self.strip_re.sub('', request.headers['cookie'])
            request.META['HTTP_COOKIE'] = cookie
        except:
            pass
