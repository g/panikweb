from datetime import datetime, timedelta

import sorl.thumbnail.base
from sorl.thumbnail import default
from sorl.thumbnail.images import ImageFile
from sorl.thumbnail.parsers import parse_geometry


def tofirstdayinisoweek(year, week):
    # from http://stackoverflow.com/questions/5882405/get-date-from-iso-week-number-in-python
    ret = datetime.strptime('%04d-%02d-1' % (year, week), '%Y-%W-%w')
    if datetime(year, 1, 4).isoweekday() > 4:
        ret -= timedelta(days=7)
    return ret


class ThumbnailBackend(sorl.thumbnail.base.ThumbnailBackend):
    def _create_alternative_resolutions(self, source_image, geometry_string, options, name):
        super()._create_alternative_resolutions(source_image, geometry_string, options, name)
        # create .webp alternative, to be served to appropriate browsers
        ratio = default.engine.get_image_ratio(source_image, options)
        geometry = parse_geometry(geometry_string, ratio)
        options = options.copy()
        options['format'] = 'WEBP'
        image = default.engine.create(source_image, geometry, options)
        thumbnail_name = '%(file_name)s.webp' % {'file_name': name}
        thumbnail = ImageFile(thumbnail_name, default.storage)
        default.engine.write(image, options, thumbnail)
        size = default.engine.get_image_size(image)
        thumbnail.set_size(size)
