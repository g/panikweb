#!/usr/bin/env python

import os

import django.conf.global_settings as DEFAULT_SETTINGS

DEBUG = True  # Turn off for production
DEBUG_TOOLBAR = False

PROJECT_DIR = os.path.normpath(os.path.dirname(os.path.dirname(__file__)))
PROJECT_PATH = os.path.dirname(os.path.dirname(__file__))

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)
MANAGERS = ADMINS
DEFAULT_FROM_EMAIL = 'info@example.net'

LOGIN_REDIRECT_URL = '/'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_DIR, 'panikweb.sqlite3'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    },
}

ACCOUNT_ACTIVATION_DAYS = 7  # Activation window

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Brussels'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-be'

LANGUAGES = (
    ('fr', 'Français'),
    ('en', 'English'),
)

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

LOCALE_PATHS = (os.path.join(PROJECT_DIR, 'panikweb', 'locale'),)

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(PROJECT_DIR, 'staticroot')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'gadjo.finders.XStaticFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)


# Make this unique, and don't share it with anybody.
SECRET_KEY = '3qm&amp;@6264-=st16)7_xa*ds+31e0mqqs@+*!ud7gzt$tq!b^qn'

MIDDLEWARE = (
    'django.middleware.cache.UpdateCacheMiddleware',
    #'django.middleware.gzip.GZipMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    #'request.middleware.RequestMiddleware',
    'panikweb.middleware.StripPiwikCookieMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'panikweb.context_processors.panikweb',
            ],
            'builtins': [
                'combo.public.templatetags.combo',
            ],
        },
    },
]


ROOT_URLCONF = 'panikweb.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'haystack',
    'taggit',
    'gadjo',
    'panikweb.base',
    'panikweb.paniktags',
    'panikweb.webstats',
    'sorl.thumbnail',
    'ckeditor',
    'emissions',
    'newsletter',
    'nonstop',
    'combo.data',
    'combo.public',
    'combo.profile',
    'combo.apps.assets',
    'combo.apps.dashboard',
    'combo.apps.export_import',
    'combo.apps.gallery',
    'combo.apps.maps',
    'combo.apps.notifications',
    'combo.apps.pwa',
    'combo.apps.search',
    'panikombo',
    'publik_django_templatetags',
    'xstatic.pkg.leaflet',
    'xstatic.pkg.leaflet_markercluster',
    'xstatic.pkg.leaflet_gesturehandling',
)

CKEDITOR_UPLOAD_PATH = os.path.join(MEDIA_ROOT, 'uploads')
CKEDITOR_UPLOAD_PREFIX = '/media/uploads/'

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.solr_backend.SolrEngine',
        'URL': 'http://127.0.0.1:8985/solr/panik',
    },
}

ENABLE_PIWIK = False

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    },
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {'require_debug_false': {'()': 'django.utils.log.RequireDebugFalse'}},
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    },
}

STATSD_CLIENT = 'django_statsd.clients.null'

RAVEN_CONFIG = None

ACCESS_LOG_FILENAME = '/var/log/nginx/panikweb-access.log'

from panikombo.misc import COMBO_CELL_TEMPLATES, COMBO_PUBLIC_TEMPLATES

LANGUAGE_COOKIE_NAME = 'panikweb_language'

TEMPLATE_VARS = {
    'pwa_display': 'standalone',
    'theme_color': "#000000",
}

COMBO_DASHBOARD_ENABLED = False
COMBO_MAP_TILE_URLTEMPLATE = ''
COMBO_MAP_ATTRIBUTION = ''
JSON_CELL_TYPES = {}
COMBO_CELL_ASSET_SLOTS = {}

RADIO_NAME = 'Example Radio'
RADIO_LONG_NAME = 'Example Radio'
RADIO_META_DESCRIPTION = None
RADIO_META_DEFAULT_IMAGE_PATH = None
WEBSITE_BASE_URL = 'https://www.example.net/'
PODCASTS_DEFAULT_IMAGE_PATH = ''
PODCASTS_PUBLICATION_DELAY = 30  # minutes

# list of radio streams
# ex: [{'id': 'ogg', 'm3u_url': '...', 'url': '...', 'type': 'audio/ogg'}]
RADIO_STREAM_URLS = []

HOME_EMISSIONS_COUNT = 3
HOME_FOCUS_COUNT = 3
HOME_NEWSITEMS_COUNT = 3
HOME_PODCASTS_COUNT = 3
USE_AGENDA_ONLY_FIELD = False

THUMBNAIL_PRESERVE_FORMAT = True

# dimension and options as accepted by sorl.thumbnail
# ex:
# * PODCAST_IMAGE_GEOMETRY = '400x400'
# * PODCAST_IMAGE_THUMBNAIL_OPTIONS = {'upscale': False, 'crop': '50% 25%'}
PODCAST_IMAGE_GEOMETRY = None
PODCAST_IMAGE_THUMBNAIL_OPTIONS = {}

ONAIR_ALWAYS_INCLUDE_TRACK_METADATA = False

# allow to serve from an alternative path if file is not found locally,
# to be used with nginx, sth like
#  location /remote_media {
#    internal;
#    proxy_pass https://panikdb.radiopanik.org/media/;
#    proxy_limit_rate 500k;
#  }
#  location /local_media {
#    internal;
#    alias /srv/www.radiopanik.org/media;
#  }
# with OFFSITE_MEDIA_SOUNDS = ('/local_media/', '/remote_media/')
OFFSITE_MEDIA_SOUNDS = None

# base URL for offloaded files, optional, only used to get statistics (must include media/)
OFFSITE_BASE_URL = None

PROGRAM_PREFIX = 'programme/'
EMISSIONS_PREFIX = 'emissions/'
NEWSITEMS_PREFIX = 'actus/'

THUMBNAIL_BACKEND = 'panikweb.utils.ThumbnailBackend'

COMBO_INITIAL_LOGIN_PAGE_PATH = '/'
COMBO_WELCOME_PAGE_PATH = None
CELL_CONDITIONS_ENABLED = False

FTS_DICTIONARY_CONFIG = 'french'
USE_HAYSTACK = False

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

local_settings_file = os.environ.get(
    'PANIKWEB_SETTINGS_FILE', os.path.join(os.path.dirname(__file__), 'local_settings.py')
)
if os.path.exists(local_settings_file):
    exec(open(local_settings_file).read())

if DEBUG and DEBUG_TOOLBAR:
    MIDDLEWARE += ('debug_toolbar.middleware.DebugToolbarMiddleware',)
    INSTALLED_APPS += ('debug_toolbar',)

if ENABLE_PIWIK is False:
    MIDDLEWARE = tuple(x for x in MIDDLEWARE if x != 'panikweb.middleware.StripPiwikCookieMiddleware')

if STATSD_CLIENT != 'django_statsd.clients.null':
    MIDDLEWARE = (
        'django_statsd.middleware.GraphiteRequestTimingMiddleware',
        'django_statsd.middleware.GraphiteMiddleware',
    ) + MIDDLEWARE
    INSTALLED_APPS += ('django_statsd',)

if RAVEN_CONFIG:
    INSTALLED_APPS += ('raven.contrib.django.raven_compat',)
