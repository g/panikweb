import math
import os
import random
import stat
import urllib.parse
from datetime import date, datetime, time, timedelta

import pkg_resources
from combo.data.models import Page
from combo.public.views import publish_page
from django.conf import settings
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.syndication.views import Feed, add_domain
from django.core.files.storage import default_storage
from django.core.paginator import Paginator
from django.db.models import Q
from django.http import Http404, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.encoding import force_str
from django.utils.feedgenerator import Atom1Feed, Rss201rev2Feed
from django.utils.timezone import is_naive, make_aware
from django.views.decorators.cache import cache_control
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import RedirectView, View
from django.views.generic.base import TemplateView
from django.views.generic.dates import MonthArchiveView, _date_from_string
from django.views.generic.detail import DetailView
from emissions.app_settings import app_settings as emissions_app_settings
from emissions.models import (
    Category,
    Diffusion,
    Emission,
    Episode,
    Focus,
    NewsCategory,
    NewsItem,
    Nonstop,
    Schedule,
    SoundFile,
)
from emissions.utils import period_program, whatsonair
from emissions.views import EmissionEpisodeMixin
from haystack.query import SearchQuerySet
from newsletter.forms import SubscribeForm
from nonstop.models import SomaLogLine
from nonstop.utils import get_current_nonstop_track
from panikombo.models import ItemTopik
from sorl.thumbnail.shortcuts import get_thumbnail

from . import utils


def check_valid_year(year):
    if year < 1983 or year > (datetime.today().year + 3):
        raise Http404()


class EmissionMixin:
    def get_emission_context(self, emission, episode_ids=None):
        context = {}

        # get all episodes
        episodes_queryset = Episode.objects.select_related()
        if episode_ids is not None:
            episodes_queryset = episodes_queryset.filter(id__in=episode_ids)
        else:
            episodes_queryset = episodes_queryset.filter(emission=emission)

        if settings.USE_AGENDA_ONLY_FIELD:
            episodes_queryset = episodes_queryset.exclude(agenda_only=True)

        context['episodes'] = episodes_queryset.filter(first_diffusion_date__lte=datetime.now()).order_by(
            '-first_diffusion_date'
        )

        context['all_episodes'] = episodes_queryset.order_by('-first_diffusion_date')

        context['futurEpisodes'] = episodes_queryset.filter(first_diffusion_date__gt=datetime.now()).order_by(
            'first_diffusion_date'
        )

        # get all related soundfiles in a single query
        soundfiles = {}
        if episode_ids is not None:
            for episode_id in episode_ids:
                soundfiles[episode_id] = None
        else:
            for episode in Episode.objects.filter(emission=emission):
                soundfiles[episode.id] = None

        for soundfile in (
            SoundFile.objects.published().select_related().filter(fragment=False, episode__emission=emission)
        ):
            soundfiles[soundfile.episode_id] = soundfile

        Episode.set_prefetched_soundfiles(soundfiles)

        return context


class EmissionDetailView(DetailView, EmissionMixin):
    model = Emission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['schedules'] = (
            Schedule.objects.select_related().filter(emission=self.object).order_by('rerun', 'datetime')
        )
        context['news'] = (
            NewsItem.objects.all()
            .filter(emission=self.object.id)
            .exclude(expiration_date__lt=date.today())  # expiration date
            .exclude(date__lt=date.today() - timedelta(days=60))
            .order_by('-date')[:3]
        )
        try:
            nonstop_object = Nonstop.objects.get(slug=self.object.slug)
        except Nonstop.DoesNotExist:
            pass
        else:
            today = date.today()
            dates = [today - timedelta(days=x) for x in range(7)]
            if datetime.now().time() < nonstop_object.start:
                dates = dates[1:]
            context['nonstop'] = nonstop_object
            context['nonstop_dates'] = dates
        context.update(self.get_emission_context(self.object))
        return context


emission = EmissionDetailView.as_view()


class EpisodeDetailView(EmissionEpisodeMixin, DetailView, EmissionMixin):
    model = Episode

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['diffusions'] = (
            Diffusion.objects.select_related().filter(episode=self.object.id).order_by('datetime')
        )
        try:
            context['emission'] = context['episode'].emission
        except Emission.DoesNotExist:
            raise Http404()
        if self.kwargs.get('emission_slug') != context['emission'].slug:
            raise Http404()
        context.update(self.get_emission_context(context['emission']))
        context['topik_pages'] = [x.page for x in ItemTopik.objects.filter(episode=self.object)]
        return context


episode = EpisodeDetailView.as_view()


class NonstopPlaylistView(TemplateView):
    template_name = 'nonstop_playlist.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context['date'] = date(int(kwargs.get('year')), int(kwargs.get('month')), int(kwargs.get('day')))
        except ValueError:
            raise Http404()
        check_valid_year(context['date'].year)
        context['future'] = context['date'] >= date.today()

        context['emission'] = Emission.objects.filter(slug=kwargs.get('slug')).first()
        try:
            nonstop_object = Nonstop.objects.get(slug=kwargs.get('slug'))
        except Nonstop.DoesNotExist:
            raise Http404()
        context['nonstop'] = nonstop_object
        start = datetime(
            int(kwargs.get('year')),
            int(kwargs.get('month')),
            int(kwargs.get('day')),
            nonstop_object.start.hour,
            nonstop_object.start.minute,
        )
        end = datetime(
            int(kwargs.get('year')),
            int(kwargs.get('month')),
            int(kwargs.get('day')),
            nonstop_object.end.hour,
            nonstop_object.end.minute,
        )
        if end < start:
            end = end + timedelta(days=1)
        context['lines'] = (
            SomaLogLine.objects.filter(play_timestamp__gte=start, play_timestamp__lte=end)
            .exclude(on_air=False)
            .select_related()
        )
        return context


nonstop_playlist = NonstopPlaylistView.as_view()


class EmissionEpisodesDetailView(DetailView, EmissionMixin):
    model = Emission
    template_name = 'emissions/episodes.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['schedules'] = (
            Schedule.objects.select_related().filter(emission=self.object).order_by('rerun', 'datetime')
        )
        count_per_month = 0
        for schedule in context['schedules']:
            if schedule.rerun:
                continue
            count_per_month += bin(schedule.weeks).count('1')

        context['count_per_month'] = count_per_month

        context['search_query'] = self.request.GET.get('q')
        if context['search_query']:
            if settings.USE_HAYSTACK:
                # query string
                sqs = (
                    SearchQuerySet()
                    .models(Episode)
                    .filter(emission_slug_exact=self.object.slug, text=context['search_query'])
                )
                episode_ids = [x.pk for x in sqs]
            else:
                vector = SearchVector(
                    'title', config=settings.FTS_DICTIONARY_CONFIG, weight='A'
                ) + SearchVector('text', config=settings.FTS_DICTIONARY_CONFIG, weight='B')
                query = SearchQuery(context['search_query'], config=settings.FTS_DICTIONARY_CONFIG)
                qs = Episode.objects.filter(emission=self.object)
                qs = qs.annotate(rank=SearchRank(vector, query)).filter(rank__gte=0.1).order_by('-rank')
                episode_ids = qs.values_list('id', flat=True)
        else:
            episode_ids = None

        context.update(self.get_emission_context(self.object, episode_ids=episode_ids))
        return context


emissionEpisodes = EmissionEpisodesDetailView.as_view()


class SoundFileEmbedView(DetailView):
    model = SoundFile
    template_name = 'soundfiles/embed.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.kwargs.get('episode_slug') != self.object.episode.slug:
            raise Http404()
        if self.kwargs.get('emission_slug') != self.object.episode.emission.slug:
            raise Http404()
        context['episode'] = self.object.episode
        return context


soundfile_embed = SoundFileEmbedView.as_view()


class EpisodeEmbedRedirect(RedirectView):
    def get_redirect_url(self, **kwargs):
        try:
            soundfile = SoundFile.objects.published().get(
                episode__slug=kwargs['episode_slug'],
                episode__emission__slug=kwargs['emission_slug'],
                fragment=False,
            )
        except SoundFile.DoesNotExist:
            raise Http404()
        kwargs['pk'] = soundfile.id
        return reverse('soundfile-embed-view', kwargs=kwargs)


episode_embed_redirect = EpisodeEmbedRedirect.as_view()


class SoundFileDialogEmbedView(DetailView):
    model = SoundFile
    template_name = 'soundfiles/dialog-embed.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.kwargs.get('episode_slug') != self.object.episode.slug:
            raise Http404()
        if self.kwargs.get('emission_slug') != self.object.episode.emission.slug:
            raise Http404()
        context['episode'] = self.object.episode
        return context


soundfile_dlg_embed = SoundFileDialogEmbedView.as_view()


class ProgramView(TemplateView):
    template_name = 'program.html'

    def get_context_data(self, year=None, week=None, **kwargs):
        context = super().get_context_data(**kwargs)

        context['weekday'] = datetime.today().weekday()

        context['week'] = week = int(week) if week is not None else datetime.today().isocalendar()[1]
        context['year'] = year = int(year) if year is not None else datetime.today().isocalendar()[0]
        if context['week'] > 53:
            raise Http404()
        check_valid_year(context['year'])
        context['week_first_day'] = utils.tofirstdayinisoweek(year, week)
        context['week_last_day'] = context['week_first_day'] + timedelta(days=6)

        return context


program = ProgramView.as_view()


class TimeCell:
    nonstop = None
    w = 1
    h = 1
    time_label = None

    def __init__(self, i, j):
        self.x = i
        self.y = j
        self.schedules = []

    def add_schedule(self, schedule):
        same_emission_and_duration = [
            x
            for x in self.schedules
            if (x.emission_id == schedule.emission_id and x.get_duration() == schedule.get_duration())
        ]
        if same_emission_and_duration:
            # add extra week/s to existing schedule
            same_emission_and_duration[0].weeks |= schedule.weeks
            return
        end_time = schedule.datetime + timedelta(minutes=schedule.get_duration())
        self.time_label = '%02d:%02d-%02d:%02d' % (
            schedule.datetime.hour,
            schedule.datetime.minute,
            end_time.hour,
            end_time.minute,
        )
        self.schedules.append(schedule)

    def sorted_schedules(self):
        return sorted(self.schedules, key=lambda x: x.week_sort_key())

    def __str__(self):
        if self.schedules:
            return ', '.join([x.emission.title for x in self.schedules])
        else:
            return self.nonstop

    def __eq__(self, other):
        return force_str(self) == force_str(other) and self.time_label == other.time_label


class Grid(TemplateView):
    template_name = 'grid.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        nb_lines = 2 * 24  # the cells are half hours
        grid = []

        times = ['%02d:%02d' % (x / 2, x % 2 * 30) for x in range(nb_lines)]
        # start grid after the night programs
        times = (
            times[
                2 * emissions_app_settings.DAY_HOUR_START
                + (1 if emissions_app_settings.DAY_MINUTE_START else 0) :
            ]
            + times[
                : 2 * emissions_app_settings.DAY_HOUR_START
                + (1 if emissions_app_settings.DAY_MINUTE_START else 0)
            ]
        )

        nonstops = []
        for nonstop in Nonstop.objects.all():
            if nonstop.start == nonstop.end:
                continue
            if nonstop.start < nonstop.end:
                nonstops.append(
                    [
                        nonstop.start.hour + nonstop.start.minute / 60.0,
                        nonstop.end.hour + nonstop.end.minute / 60.0,
                        nonstop.get_public_label(),
                        nonstop.slug,
                        nonstop,
                    ]
                )
            else:
                # crossing midnight
                nonstops.append(
                    [
                        nonstop.start.hour + nonstop.start.minute / 60.0,
                        24,
                        nonstop.get_public_label(),
                        nonstop.slug,
                        nonstop,
                    ]
                )
                nonstops.append(
                    [
                        0,
                        nonstop.end.hour + nonstop.end.minute / 60.0,
                        nonstop.get_public_label(),
                        nonstop.slug,
                        nonstop,
                    ]
                )
        nonstops.sort()

        for i in range(nb_lines):
            grid.append([])
            for j in range(7):
                grid[-1].append(TimeCell(i, j))

            try:
                nonstop = [x for x in nonstops if i >= x[0] * 2 and i < x[1] * 2][0]
            except IndexError:
                nonstop = [0, 24, '', '', None]
            for time_cell in grid[-1]:
                time_cell.nonstop = nonstop[2]
                time_cell.nonstop_slug = nonstop[3]
                time_cell.redirect_path = nonstop[4].redirect_path if nonstop[4] else None
                if (
                    nonstop[1]
                    == emissions_app_settings.DAY_HOUR_START + emissions_app_settings.DAY_MINUTE_START / 60
                ):
                    # the one ending at dawn will be cut down, so we inscribe
                    # its duration manually
                    time_cell.time_label = '%02d:00-%02d:%02d' % (
                        nonstop[0],
                        nonstop[1],
                        emissions_app_settings.DAY_MINUTE_START,
                    )

        for schedule in (
            Schedule.objects.exclude(info_only_schedule=True)
            .filter(Q(start_date__isnull=True) | Q(start_date__lte=date.today()))
            .filter(Q(end_date__isnull=True) | Q(end_date__gt=date.today()))
            .prefetch_related('emission__categories')
            .select_related()
            .order_by('datetime')
        ):
            row_start = schedule.datetime.hour * 2 + int(math.ceil(schedule.datetime.minute / 30))
            if schedule.get_duration() < 30:
                # special case for an emission during 12:45-13:00
                row_start = schedule.datetime.hour * 2 + int(math.floor(schedule.datetime.minute / 30))
            day_no = schedule.get_weekday()

            for step in range(int(math.ceil(schedule.get_duration() / 30.0))):
                if grid[(row_start + step) % nb_lines][day_no] is None:
                    grid[(row_start + step) % nb_lines][day_no] = TimeCell()
                grid[(row_start + step) % nb_lines][day_no].add_schedule(schedule)

        # start grid after the night programs
        grid = (
            grid[
                2 * emissions_app_settings.DAY_HOUR_START
                + (1 if emissions_app_settings.DAY_MINUTE_START else 0) :
            ]
            + grid[
                : 2 * emissions_app_settings.DAY_HOUR_START
                + (1 if emissions_app_settings.DAY_MINUTE_START else 0)
            ]
        )

        # look for the case where the same emission has different schedules for
        # the same time cell, for example if it lasts one hour the first week,
        # and two hours the third week.
        for i in range(nb_lines):
            grid[i] = [x for x in grid[i] if x is not None]
            for j, cell in enumerate(grid[i]):
                if grid[i][j] is None:
                    continue
                if len(grid[i][j].schedules) > 1:
                    time_cell_emissions = {}
                    for schedule in grid[i][j].schedules:
                        if not schedule.emission.id in time_cell_emissions:
                            time_cell_emissions[schedule.emission.id] = []
                        time_cell_emissions[schedule.emission.id].append(schedule)
                    for schedule_list in time_cell_emissions.values():
                        if len(schedule_list) == 1:
                            continue
                        # here it is, same cell, same emission, several
                        # schedules
                        schedule_list.sort(key=lambda x: x.get_duration())

                        schedule = schedule_list[0]
                        end_time = schedule.datetime + timedelta(minutes=schedule.get_duration())
                        grid[i][j].time_label = '%02d:%02d-%02d:%02d' % (
                            schedule.datetime.hour,
                            schedule.datetime.minute,
                            end_time.hour,
                            end_time.minute,
                        )

                        schedule_list.sort(key=lambda x: x.weeks)
                        for schedule in schedule_list[1:]:
                            grid[i][j].schedules.remove(schedule)
                            end_time = schedule.datetime + timedelta(minutes=schedule.get_duration())
                            if schedule_list[0].get_duration() == schedule.get_duration():
                                # same duration, append week info
                                schedule_list[0].time_label_extra = ', %s' % (schedule.weeks_string,)
                            else:
                                # different durations, also append other
                                # endtime info
                                schedule_list[0].time_label_extra = ', -%02d:%02d %s' % (
                                    end_time.hour,
                                    end_time.minute,
                                    schedule.weeks_string,
                                )
                        pass

        # merge adjacent

        # 1st thing is to merge cells on the same line, this will mostly catch
        # consecutive nonstop cells
        for i in range(nb_lines):
            for j, cell in enumerate(grid[i]):
                if grid[i][j] is None:
                    continue
                t = 1
                try:
                    # if the cells are identical, they are removed from the
                    # grid, and current cell width is increased
                    while grid[i][j + t] == cell:
                        cell.w += 1
                        grid[i][j + t] = None
                        t += 1
                except IndexError:
                    pass

            # once we're done we remove empty cells
            grid[i] = [x for x in grid[i] if x is not None]

        # 2nd thing is to merge cells vertically, this is emissions that last
        # for more than 30 minutes
        for i in range(nb_lines):
            grid[i] = [x for x in grid[i] if x is not None]
            for j, cell in enumerate(grid[i]):
                if grid[i][j] is None:
                    continue
                t = 1
                try:
                    while True:
                        # we look if the next time cell has the same emissions
                        same_cell_below = [
                            (bj, x)
                            for bj, x in enumerate(grid[i + cell.h])
                            if x == cell and x.y == cell.y and x.w == cell.w
                        ]
                        if same_cell_below:
                            # if the cell was identical, we remove it and
                            # increase current cell height
                            bj, same_cell_below = same_cell_below[0]
                            del grid[i + cell.h][bj]
                            cell.h += 1
                        else:
                            # if the cell is different, we have a closer look
                            # to it, so we can remove emissions that will
                            # already be mentioned in the current cell.
                            #
                            # For example:
                            #  - 7am30, seuls contre tout, 1h30
                            #  - 8am, du pied gauche & la voix de la rue, 1h
                            # should produce: (this is case A)
                            #  |      7:30-9:00      |
                            #  |  seuls contre tout  |
                            #  |---------------------|
                            #  |      8:00-9:00      |
                            #  |   du pied gauche    |
                            #  |  la voix de la rue  |
                            #
                            # On the other hand, if all three emissions started
                            # at 7am30, we want: (this is case B)
                            #  |      7:30-9:00      |
                            #  |  seuls contre tout  |
                            #  |   du pied gauche    |
                            #  |  la voix de la rue  |
                            # that is we merge all of them, ignoring the fact
                            # that the other emissions will stop at 8am30
                            current_cell_schedules = set(grid[i][j].schedules)
                            current_cell_emissions = {x.emission for x in current_cell_schedules}
                            cursor = 1
                            while True and current_cell_schedules:
                                same_cell_below = [x for x in grid[i + cursor] if x.y == grid[i][j].y]
                                if not same_cell_below:
                                    cursor += 1
                                    continue
                                same_cell_below = same_cell_below[0]
                                same_cell_below_emissions = {x.emission for x in same_cell_below.schedules}

                                if current_cell_emissions.issubset(same_cell_below_emissions):
                                    # this handles case A (see comment above)
                                    for schedule in current_cell_schedules:
                                        if (
                                            schedule in same_cell_below.schedules
                                            and len(same_cell_below.schedules) == 1
                                        ):
                                            # len(same_cell_below.schedules) == 1 would mean
                                            # we were in this situation
                                            # * 7:00-9:00 emissionA
                                            # * 7:30-8:30 emissionB, in the middle
                                            # in that case we do not want 8:30-9:00 cell to get empty
                                            # but we do not want to repeat the time label
                                            same_cell_below.time_label = ''
                                        elif schedule in same_cell_below.schedules:
                                            same_cell_below.schedules.remove(schedule)

                                elif same_cell_below_emissions and current_cell_emissions.issuperset(
                                    same_cell_below_emissions
                                ):
                                    # this handles case B (see comment above)
                                    # we set the cell time label to the longest
                                    # period
                                    grid[i][j].time_label = same_cell_below.time_label
                                    # then we sort emissions so the longest are
                                    # put first
                                    grid[i][j].schedules.sort(key=lambda x: -x.get_duration())
                                    # then we add individual time labels to the
                                    # other schedules
                                    for schedule in current_cell_schedules:
                                        if schedule not in same_cell_below.schedules:
                                            end_time = schedule.datetime + timedelta(
                                                minutes=schedule.get_duration()
                                            )
                                            schedule.time_label = '%02d:%02d-%02d:%02d' % (
                                                schedule.datetime.hour,
                                                schedule.datetime.minute,
                                                end_time.hour,
                                                end_time.minute,
                                            )
                                    grid[i][j].h += 1
                                    grid[i + cursor].remove(same_cell_below)
                                elif same_cell_below_emissions and current_cell_emissions.intersection(
                                    same_cell_below_emissions
                                ):
                                    same_cell_below.schedules = [
                                        x
                                        for x in same_cell_below.schedules
                                        if x.emission not in current_cell_emissions or x.get_duration() < 30
                                    ]
                                else:
                                    # totally different cell, stop here
                                    break
                                cursor += 1
                            break
                except IndexError:
                    pass

        # cut late night hours
        grid = grid[:44]
        times = times[:44]

        context['grid'] = grid
        context['times'] = times
        context['categories'] = Category.objects.all()
        # dates from Monday to Sunday
        context['weekdays'] = [date(2018, 1, x) for x in range(1, 8)]

        return context


grid = Grid.as_view()


class Home(TemplateView):
    template_name = 'home.html'

    def dispatch(self, request, *args, **kwargs):
        page = Page.objects.filter(slug='index', parent__isnull=True).first()
        if page:
            request.extra_context_data = {'absolute_uri': request.build_absolute_uri()}
            return publish_page(request, page)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['emissions'] = Emission.objects.filter(archived=False).order_by('-creation_timestamp')[
            : settings.HOME_EMISSIONS_COUNT
        ]
        context['newsitems'] = NewsItem.objects.exclude(date__gt=date.today()).order_by('-date')[
            : settings.HOME_NEWSITEMS_COUNT
        ]

        context['soundfiles'] = (
            SoundFile.objects.published()
            .prefetch_related('episode__emission__categories')
            .filter(fragment=False)
            .filter(episode__first_diffusion_date__lt=datetime.now())
            .filter(
                creation_timestamp__lt=datetime.now() - timedelta(minutes=settings.PODCASTS_PUBLICATION_DELAY)
            )
            .select_related()
            .order_by('-episode__first_diffusion_date')[: settings.HOME_PODCASTS_COUNT]
        )

        context['newsletter_form'] = SubscribeForm()

        return context


home = Home.as_view()


class NewsItemView(DetailView):
    model = NewsItem

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = NewsCategory.objects.all()
        context['news'] = NewsItem.objects.exclude(date__gt=date.today()).order_by('-date')
        context['topik_pages'] = [x.page for x in ItemTopik.objects.filter(newsitem=self.object)]
        return context


newsitemview = NewsItemView.as_view()


class News(TemplateView):
    template_name = 'news.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['focus'] = (
            NewsItem.objects.exclude(date__gt=date.today())  # publication date
            .exclude(expiration_date__lt=date.today())  # expiration date
            .filter(got_focus__isnull=False)
            .select_related('category')
            .order_by('-date')[:10]
        )
        context['news'] = NewsItem.objects.exclude(date__gt=date.today()).order_by('-date')
        context['news_not_expired'] = (
            NewsItem.objects.exclude(date__gt=date.today())
            .exclude(expiration_date__lt=date.today())
            .order_by('-date')
        )
        return context


news = News.as_view()


class Agenda(TemplateView):
    template_name = 'agenda.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agenda'] = (
            NewsItem.objects.exclude(date__gt=date.today())
            .filter(event_date__gte=date.today())
            .order_by('event_date')[:20]
        )
        context['news'] = NewsItem.objects.exclude(date__gt=date.today()).order_by('-date')
        context['previous_month'] = datetime.today().replace(day=1) - timedelta(days=2)
        return context


agenda = Agenda.as_view()


class AgendaByMonth(MonthArchiveView):
    template_name = 'agenda.html'
    queryset = NewsItem.objects.filter(event_date__isnull=False)
    allow_future = True
    date_field = 'event_date'
    month_format = '%m'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agenda'] = context['object_list']
        context['news'] = NewsItem.objects.all().order_by('-date')
        return context


agenda_by_month = AgendaByMonth.as_view()


class Emissions(TemplateView):
    template_name = 'emissions.html'

    def get_queryset(self):
        return Emission.objects.prefetch_related('categories').filter(archived=False).order_by('title')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['emissions'] = self.get_queryset()
        context['categories'] = Category.objects.all()
        return context


emissions = Emissions.as_view()


class EmissionsArchives(TemplateView):
    template_name = 'emissions/archives.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['emissions'] = (
            Emission.objects.prefetch_related('categories').filter(archived=True).order_by('title')
        )
        context['categories'] = Category.objects.all()
        return context


emissionsArchives = EmissionsArchives.as_view()


class Listen(TemplateView):
    template_name = 'listen.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['focus'] = (
            SoundFile.objects.published()
            .prefetch_related('episode__emission__categories')
            .filter(got_focus__isnull=False)
            .select_related()
            .order_by('-episode__first_diffusion_date')[:10]
        )
        context['soundfiles'] = (
            SoundFile.objects.published()
            .prefetch_related('episode__emission__categories')
            .select_related()
            .order_by('-creation_timestamp')[:20]
        )

        return context


listen = Listen.as_view()


class OnAir(View):
    def get_infos(self, content_filter):
        ctx = whatsonair(content_filter=content_filter)
        infos = {}
        include_track_metadata = settings.ONAIR_ALWAYS_INCLUDE_TRACK_METADATA

        if ctx.get('episode'):
            infos['episode'] = {
                'title': ctx['episode'].title,
                'subtitle': ctx['episode'].subtitle,
                'url': ctx['episode'].get_absolute_url(),
            }

        if ctx.get('emission'):
            chat_url = None
            if ctx['emission'].chat_open:
                chat_url = reverse('emission-chat', kwargs={'slug': ctx['emission'].slug})
            infos['emission'] = {
                'title': ctx['emission'].title,
                'subtitle': ctx['emission'].subtitle,
                'slug': ctx['emission'].slug,
                'url': ctx['emission'].get_absolute_url(),
                'chat': chat_url,
            }
            if (
                hasattr(ctx['current_slot'], 'recurringplaylistdiffusion_set')
                and ctx['current_slot'].recurringplaylistdiffusion_set.exists()
            ):
                include_track_metadata = True

        if ctx.get('nonstop'):
            include_track_metadata = True
            redirect_path = ctx['nonstop'].redirect_path
            infos['nonstop'] = {
                'title': ctx['nonstop'].get_public_label(),
                'slug': ctx['current_slot'].slug,
            }
            if redirect_path:
                infos['nonstop']['url'] = redirect_path
            today = datetime.today()
            infos['nonstop']['playlist_url'] = reverse(
                'nonstop-playlist',
                kwargs={
                    'year': today.year,
                    'month': today.month,
                    'day': today.day,
                    'slug': ctx['current_slot'].slug,
                },
            )

        if include_track_metadata:
            infos.update(get_current_nonstop_track(content_filter=content_filter))

        return infos

    def get(self, request, *args, **kwargs):
        infos = self.get_infos(content_filter=request.GET.get('content_filter'))
        return JsonResponse({'data': infos})


onair = cache_control(max_age=15)(csrf_exempt(OnAir.as_view()))


class DabService(View):
    def get_infos(self, ctx):
        infos = {'text1': '', 'text2': '', 'text3': ''}
        include_track_metadata = settings.ONAIR_ALWAYS_INCLUDE_TRACK_METADATA

        if ctx.get('episode'):
            infos['text1'] = ctx['episode'].title
            infos['text2'] = ctx['emission'].title
        elif ctx.get('emission'):
            infos['text1'] = ctx['emission'].title
            if (
                hasattr(ctx['current_slot'], 'recurringplaylistdiffusion_set')
                and ctx['current_slot'].recurringplaylistdiffusion_set.exists()
            ):
                include_track_metadata = True
        elif ctx.get('nonstop'):
            include_track_metadata = True

        if include_track_metadata:
            track_info = get_current_nonstop_track()
            if track_info.get('track_artist'):
                infos['text1'] = track_info['track_artist']
                infos['text2'] = track_info['track_title']
            elif track_info.get('track_title'):
                infos['text1'] = track_info['track_title']
            elif ctx.get('nonstop'):
                infos['text1'] = ctx.get('nonstop').title

        return infos

    def get(self, request, *args, **kwargs):
        infos = self.get_infos(ctx=whatsonair())
        return JsonResponse({'data': infos})


dab_service = cache_control(max_age=15)(csrf_exempt(DabService.as_view()))


class NewsItemDetailView(DetailView):
    model = NewsItem


newsitem = NewsItemDetailView.as_view()


class RssCustomPodcastsFeed(Rss201rev2Feed):
    def add_root_elements(self, handler):
        super().add_root_elements(handler)
        emission = self.feed.get('emission')
        if emission and emission.image and emission.image.url:
            if settings.PODCAST_IMAGE_GEOMETRY:
                image_url = get_thumbnail(
                    emission.image,
                    settings.PODCAST_IMAGE_GEOMETRY,
                    **settings.PODCAST_IMAGE_THUMBNAIL_OPTIONS,
                ).url
            else:
                image_url = emission.image.url
        else:
            image_url = settings.PODCASTS_DEFAULT_IMAGE_PATH
        image_url = urllib.parse.urljoin(self.feed['link'], image_url)
        handler.startElement('image', {})
        if emission:
            handler.addQuickElement('title', emission.title)
        else:
            handler.addQuickElement('title', settings.RADIO_NAME)
        handler.addQuickElement('url', image_url)
        handler.endElement('image')
        handler.addQuickElement('itunes:explicit', 'no')  # invidividual items will get their own value
        handler.addQuickElement('itunes:image', None, {'href': image_url})
        if emission:
            if emission.subtitle:
                handler.addQuickElement('itunes:subtitle', emission.subtitle)
            for category in emission.categories.all():
                if category.itunes_category:
                    handler.addQuickElement('itunes:category', None, {'text': category.itunes_category})

        handler.addQuickElement('itunes:author', settings.RADIO_NAME)
        handler.startElement('itunes:owner', {})
        if emission and emission.email:
            handler.addQuickElement('itunes:email', emission.email)
        else:
            handler.addQuickElement('itunes:email', settings.DEFAULT_FROM_EMAIL)
        handler.addQuickElement('itunes:name', settings.RADIO_NAME)
        handler.endElement('itunes:owner')

    def root_attributes(self):
        attrs = super().root_attributes()
        attrs['xmlns:dc'] = 'http://purl.org/dc/elements/1.1/'
        attrs['xmlns:itunes'] = 'http://www.itunes.com/dtds/podcast-1.0.dtd'
        return attrs

    def add_item_elements(self, handler, item):
        super().add_item_elements(handler, item)
        explicit = 'no'
        for tag in item.get('tags') or []:
            handler.addQuickElement('dc:subject', tag)
            if tag == 'explicit':
                explicit = 'yes'
        if item.get('tags'):
            handler.addQuickElement('itunes:keywords', ','.join(item.get('tags')))
        handler.addQuickElement('itunes:explicit', explicit)
        episode = item.get('episode')
        if episode and episode.image and episode.image.url:
            if settings.PODCAST_IMAGE_GEOMETRY:
                image_url = get_thumbnail(
                    episode.image, settings.PODCAST_IMAGE_GEOMETRY, **settings.PODCAST_IMAGE_THUMBNAIL_OPTIONS
                ).url
            else:
                image_url = episode.image.url
            image_url = urllib.parse.urljoin(self.feed['link'], image_url)
            handler.addQuickElement('itunes:image', None, {'href': image_url})
        soundfile = item.get('soundfile')
        if soundfile.duration:
            handler.addQuickElement(
                'itunes:duration',
                '%02d:%02d:%02d'
                % (soundfile.duration / 3600, soundfile.duration % 3600 / 60, soundfile.duration % 60),
            )


class PodcastsFeed(Feed):
    title = '%s - Podcasts' % settings.RADIO_NAME
    link = '/'
    description_template = 'feed/soundfile.html'
    feed_type = RssCustomPodcastsFeed

    def get_feed(self, obj, request):
        self.request = request
        return super().get_feed(obj, request)

    @property
    def description(self):
        return settings.RADIO_META_DESCRIPTION

    def items(self):
        return (
            SoundFile.objects.published()
            .select_related()
            .filter(
                creation_timestamp__lt=datetime.now() - timedelta(minutes=settings.PODCASTS_PUBLICATION_DELAY)
            )
            .exclude(file__isnull=True)
            .exclude(file='')
            .order_by('-creation_timestamp')[:50]
        )

    def item_title(self, item):
        if item.fragment:
            return '[%s] %s - %s' % (item.episode.emission.title, item.title, item.episode.title)
        return '[%s] %s' % (item.episode.emission.title, item.episode.title)

    def item_link(self, item):
        if item.fragment:
            return item.episode.get_absolute_url() + '#%s' % item.id
        return item.episode.get_absolute_url()

    def item_enclosure_url(self, item):
        current_site = get_current_site(request=self.request)
        return add_domain(current_site.domain, item.get_format_url('mp3'), self.request.is_secure())

    def item_enclosure_length(self, item):
        if item.mp3_file_size:
            return item.mp3_file_size
        sound_path = item.get_format_path('mp3')
        try:
            return os.stat(sound_path)[stat.ST_SIZE]
        except OSError:
            return 0

    def item_enclosure_mime_type(self, item):
        return 'audio/mpeg'

    def item_pubdate(self, item):
        pub_date = item.creation_timestamp
        return make_aware(pub_date, is_dst=True) if is_naive(pub_date) else pub_date

    def item_extra_kwargs(self, item):
        return {'tags': [x.name for x in item.episode.tags.all()], 'soundfile': item, 'episode': item.episode}


podcasts_feed = PodcastsFeed()


class RssNewsFeed(Feed):
    title = settings.RADIO_NAME
    link = '/news/'
    description_template = 'feed/newsitem.html'

    def items(self):
        return NewsItem.objects.order_by('-date')[:20]

    def item_title(self, item):
        return item.title

    def item_pubdate(self, item):
        publication_datetime = datetime.combine(item.date, time(0, 0))
        pub_date = (
            publication_datetime
            if publication_datetime > item.creation_timestamp
            else item.creation_timestamp
        )
        return make_aware(pub_date, is_dst=True) if is_naive(pub_date) else pub_date


rss_news_feed = RssNewsFeed()


class Atom1FeedWithBaseXml(Atom1Feed):
    def root_attributes(self):
        root_attributes = super().root_attributes()
        scheme, netloc, path, params, query, fragment = urllib.parse.urlparse(self.feed['feed_url'])
        root_attributes['xml:base'] = urllib.parse.urlunparse((scheme, netloc, '/', params, query, fragment))
        return root_attributes


class AtomNewsFeed(RssNewsFeed):
    feed_type = Atom1FeedWithBaseXml


atom_news_feed = AtomNewsFeed()


class EmissionPodcastsFeed(PodcastsFeed):
    description_template = 'feed/soundfile.html'
    feed_type = RssCustomPodcastsFeed

    def __call__(self, request, *args, **kwargs):
        self.emission = get_object_or_404(Emission, slug=kwargs.get('slug'))
        return super().__call__(request, *args, **kwargs)

    def item_title(self, item):
        if item.fragment:
            return '%s - %s' % (item.title, item.episode.title)
        return item.episode.title

    @property
    def title(self):
        return self.emission.title

    @property
    def description(self):
        return self.emission.subtitle

    @property
    def link(self):
        return reverse('emission-view', kwargs={'slug': self.emission.slug})

    def feed_extra_kwargs(self, obj):
        return {'emission': self.emission}

    def items(self):
        return (
            SoundFile.objects.published()
            .select_related()
            .filter(episode__emission__slug=self.emission.slug)
            .order_by('-creation_timestamp')[:50]
        )


emission_podcasts_feed = EmissionPodcastsFeed()


class Party(TemplateView):
    template_name = 'party.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        t = random.choice(['newsitem'] * 2 + ['emission'] * 3 + ['soundfile'] * 1 + ['episode'] * 2)
        focus = Focus()
        if t == 'newsitem':
            focus.newsitem = (
                NewsItem.objects.exclude(image__isnull=True).exclude(image__exact='').order_by('?')[0]
            )
        elif t == 'emission':
            focus.emission = (
                Emission.objects.exclude(image__isnull=True).exclude(image__exact='').order_by('?')[0]
            )
        elif t == 'episode':
            focus.episode = (
                Episode.objects.exclude(image__isnull=True).exclude(image__exact='').order_by('?')[0]
            )
        elif t == 'soundfile':
            focus.soundfile = (
                SoundFile.objects.exclude(episode__image__isnull=True)
                .exclude(episode__image__exact='')
                .order_by('?')[0]
            )

        context['focus'] = focus

        return context


party = Party.as_view()


class Chat(DetailView, EmissionMixin):
    model = Emission
    template_name = 'chat.html'


chat = cache_control(max_age=15)(Chat.as_view())


def media_hosting(request, location, *args, **kwargs):
    local_path = default_storage.path(location)
    response = HttpResponse(content_type='')
    if os.path.exists(local_path):
        response['X-Accel-Redirect'] = settings.OFFSITE_MEDIA_SOUNDS[0] + location
    else:
        response['X-Accel-Redirect'] = settings.OFFSITE_MEDIA_SOUNDS[1] + location
    return response


def versions_json(request):
    return JsonResponse({'data': {x.project_name: x.version for x in pkg_resources.WorkingSet()}})
