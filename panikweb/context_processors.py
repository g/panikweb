from combo.apps.pwa.models import PwaSettings
from combo.data.models import Page
from combo.utils.cache import cache_during_request
from django.conf import settings


def panikweb(request):
    d = {
        'site_url': request.build_absolute_uri('/').strip('/'),
        'radio_name': settings.RADIO_NAME,
        'radio_long_name': settings.RADIO_LONG_NAME or settings.RADIO_NAME,
        'radio_meta_description': settings.RADIO_META_DESCRIPTION or '',
        'radio_meta_default_image_path': settings.RADIO_META_DEFAULT_IMAGE_PATH,
        'pwa_settings': cache_during_request(PwaSettings.singleton),
        'section': request.path.strip('/').split('/')[0],
        'navigation_pages': Page.objects.filter(parent__isnull=True, exclude_from_navigation=False),
    }
    d.update(settings.TEMPLATE_VARS)
    return d
