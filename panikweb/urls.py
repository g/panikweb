import combo.monkeypatch  # noqa pylint: disable=unused-import
from combo import plugins as combo_plugins
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path, reverse_lazy
from django.views.generic import RedirectView
from django.views.i18n import JavaScriptCatalog

from . import search, views
from .webstats.urls import urlpatterns as webstats_urlpatterns

urlpatterns = [
    path('', views.home, name='home'),
    re_path(r'^onair.json$', views.onair, name='onair'),
    re_path(r'^dab-service.json$', views.dab_service),
    re_path(r'^%s$' % settings.PROGRAM_PREFIX, views.program, name='program'),
    re_path(
        r'^%s(?P<year>\d{4})/(?P<week>\d+)/$' % settings.PROGRAM_PREFIX, views.program, name='program_week'
    ),
    path('grille', views.grid, name='grid'),
    re_path(r'^%s$' % settings.EMISSIONS_PREFIX, views.emissions, name='emissions'),
    re_path(
        r'^%s(?P<slug>[\w,-]+)/episodes/$' % settings.EMISSIONS_PREFIX,
        views.emissionEpisodes,
        name='emissionEpisodes',
    ),
    re_path(
        r'^%s(?P<slug>[\w,-]+)/podcasts.rss' % settings.EMISSIONS_PREFIX,
        views.emission_podcasts_feed,
        name='emission-podcasts',
    ),
    re_path(r'^%s(?P<slug>[\w,-]+)/chat/$' % settings.EMISSIONS_PREFIX, views.chat, name='emission-chat'),
    re_path(
        r'^%s(?P<emission_slug>[\w,-]+)/(?P<slug>[\w,-]+)/$' % settings.EMISSIONS_PREFIX,
        views.episode,
        name='episode-view',
    ),
    re_path(
        r'^%s(?P<emission_slug>[\w,-]+)/(?P<episode_slug>[\w,-]+)/embed/$' % settings.EMISSIONS_PREFIX,
        views.episode_embed_redirect,
        name='episode-embed-redirect-view',
    ),
    re_path(
        r'^%s(?P<emission_slug>[\w,-]+)/(?P<episode_slug>[\w,-]+)/embed/(?P<pk>\d+)/$'
        % settings.EMISSIONS_PREFIX,
        views.soundfile_embed,
        name='soundfile-embed-view',
    ),
    re_path(
        r'^%s(?P<emission_slug>[\w,-]+)/(?P<episode_slug>[\w,-]+)/dlg-embed/(?P<pk>\d+)/$'
        % settings.EMISSIONS_PREFIX,
        views.soundfile_dlg_embed,
        name='soundfile-dialog-embed-view',
    ),
    re_path(
        r'^%s(?P<slug>[\w,-]+)/playlist/(?P<year>[0-9]{4})-(?P<month>[0-9]+)-(?P<day>[0-9]+)/$'
        % settings.EMISSIONS_PREFIX,
        views.nonstop_playlist,
        name='nonstop-playlist',
    ),
    re_path(r'^%s(?P<slug>[\w,-]+)/$' % settings.EMISSIONS_PREFIX, views.emission, name='emission-view'),
    re_path(r'^%sarchives$' % settings.EMISSIONS_PREFIX, views.emissionsArchives, name='emissionsArchives'),
    path('sons/', RedirectView.as_view(url='/podcasts/'), name='listen'),
    path('sons-old/', views.listen),
    path(
        'podcasts/archives/',
        search.listenArchives if settings.USE_HAYSTACK else search.soundfiles_search,
        name='listenArchives',
    ),
    re_path(r'^%s$' % settings.NEWSITEMS_PREFIX, views.news, name='news'),
    re_path(r'^%sagenda/$' % settings.NEWSITEMS_PREFIX, views.agenda, name='agenda'),
    re_path(
        r'^%sagenda/(?P<year>[0-9]{4})/(?P<month>[0-9]+)/$' % settings.NEWSITEMS_PREFIX,
        views.agenda_by_month,
        name='agenda_by_month',
    ),
    re_path(
        r'^%sagenda$' % settings.NEWSITEMS_PREFIX, RedirectView.as_view(pattern_name='agenda', permanent=True)
    ),
    re_path(
        r'^%sarchives/$' % settings.NEWSITEMS_PREFIX,
        search.newsArchives if settings.USE_HAYSTACK else search.newsitems_search,
        name='newsArchives',
    ),
    re_path(r'^%s(?P<slug>[\w,-]+)$' % settings.NEWSITEMS_PREFIX, views.newsitemview, name='newsitem-view'),
    path('party', views.party, name='party'),
    path('recherche/', search.view if settings.USE_HAYSTACK else search.global_search, name='search'),
    re_path(r'^%s.rss$' % settings.NEWSITEMS_PREFIX.strip('/'), views.rss_news_feed, name='rss-feed'),
    re_path(r'^%s.atom$' % settings.NEWSITEMS_PREFIX.strip('/'), views.atom_news_feed, name='atom-feed'),
    re_path(r'^podcasts.rss$', views.podcasts_feed, name='podcasts-feed'),
    path('newsletter/', include('newsletter.urls')),
    path('__webstats/', include(webstats_urlpatterns)),
    path('__versions/', views.versions_json),
    path('jsi18n', JavaScriptCatalog.as_view(), name='javascript-catalog'),
]

urlpatterns = combo_plugins.register_plugins_urls(urlpatterns)
urlpatterns += staticfiles_urlpatterns()

if settings.OFFSITE_MEDIA_SOUNDS:
    urlpatterns += [re_path(r'^media/(?P<location>sounds/.*)', views.media_hosting)]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG_TOOLBAR:
    urlpatterns += [
        path('__debug__/', include('debug_toolbar.urls')),
    ]

urlpatterns.append(path('', include('combo.public.urls')))
