# panikweb configuration files
# this file can be edited but settings are better placed in settings.d/ files

import glob

DEBUG = False

LANGUAGE_CODE = 'fr-be'
TIME_ZONE = 'Europe/Brussels'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyMemcacheCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'panikdb',
    },
}

ALLOWED_HOSTS = ()

STATIC_ROOT = '/var/lib/panikweb/static/'
MEDIA_ROOT = '/var/lib/panikweb/media/'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {},
    'formatters': {
        'verbose': {
            'format': '{asctime} ({levelname:.1s}) [{username}] {message}',
            'style': '{',
        },
        'verbose-notime': {
            'format': ' ({levelname:.1s}) [{username}] {message}',
            'style': '{',
        },
        'simple': {
            'format': '({levelname:.1s}) {message}',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
            'level': 'INFO',
        },
        'panikweb-syslog': {
            'level': 'DEBUG',
            'class': 'logging.handlers.SysLogHandler',
            'facility': 'local6',
            'address': '/dev/log',
            'formatter': 'verbose-notime',
        },
    },
    'root': {
        'handlers': ['console'],
        'level': 'WARNING',
    },
    'loggers': {
        'panikweb': {
            'handlers': ['panikweb-syslog'],
            'level': 'DEBUG',
            'propagate': False,
        },
    },
}

for filename in sorted(glob.glob('/etc/panikweb/settings.d/*.py')):
    exec(open(filename).read())
