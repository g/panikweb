#! /bin/sh

# download and create links to required django apps
if [ -d django-panik-emissions ]
then
	(cd django-panik-emissions && git pull --rebase)
else
	git clone http://git.domainepublic.net/git/django-panik-emissions.git
	ln -s django-panik-emissions/emissions
fi

if [ -d django-panik-newsletter ]
then
	(cd django-panik-newsletter && git pull --rebase)
else
	git clone http://git.domainepublic.net/git/django-panik-newsletter.git
	ln -s django-panik-newsletter/newsletter
fi

# create virtual env
test -d venv || virtualenv --system-site-packages venv/

# create media uploads directory for ckeditor
test -d media/uploads || mkdir -p media/uploads

# install required dependencies
./venv/bin/pip install -r requirements.txt
